package com.atlassian.bitbucket.linky.rest.cloud

import com.atlassian.bitbucket.linky.rest.BitbucketRestException
import com.atlassian.bitbucket.linky.rest.PullRequest
import java.util.concurrent.CompletableFuture

interface CommitApi {
    fun relatedPullRequests(pageNumber: Int = 1): CompletableFuture<CloudPage<PullRequest>>
}

enum class PullRequestState {
    MERGED, SUPERSEDED, OPEN, DECLINED
}

class PullRequestLinksNotEnabledException(val repositoryId: RepositoryId) :
    BitbucketRestException("Pull Request commit links are not indexed in '${repositoryId.displayPresentation}' repository")

class PullRequestLinksIndexingException(val repositoryId: RepositoryId) :
    BitbucketRestException("Pull Request commit links are being indexed in '${repositoryId.displayPresentation}' repository")
