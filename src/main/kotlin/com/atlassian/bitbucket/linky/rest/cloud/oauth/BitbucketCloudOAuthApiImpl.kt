package com.atlassian.bitbucket.linky.rest.cloud.oauth

import com.atlassian.bitbucket.linky.rest.auth.AccessToken
import com.atlassian.bitbucket.linky.rest.auth.OAuthTokens.RefreshAndAccessTokens
import com.atlassian.bitbucket.linky.rest.auth.RefreshToken
import com.atlassian.bitbucket.linky.rest.cloud.BitbucketCloud
import com.atlassian.bitbucket.linky.rest.cloud.config.defaultGson
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.core.Parameters
import com.github.kittinunf.fuel.core.extensions.authentication
import com.github.kittinunf.fuel.gson.responseObject
import com.google.gson.Gson
import java.time.Instant
import java.util.concurrent.CompletableFuture

class BitbucketCloudOAuthApiImpl(
    createFuel: () -> FuelManager = { FuelManager() },
    private val gson: Gson = defaultGson(),
    customConfig: BitbucketCloudOAuthApiConfig.() -> Unit = { }
) : BitbucketCloudOAuthApi {

    private val config = BitbucketCloudOAuthApiConfig().apply(customConfig)
    private val fuel = createFuel()

    override fun requestOAuthTokens(authorizationCode: AuthorizationCode): CompletableFuture<RefreshAndAccessTokens> =
        requestTokens(
            listOf(
                "grant_type" to "authorization_code",
                "code" to authorizationCode.value
            )
        )

    override fun refreshOAuthTokens(refreshToken: RefreshToken): CompletableFuture<RefreshAndAccessTokens> =
        requestTokens(
            listOf(
                "grant_type" to "refresh_token",
                "refresh_token" to refreshToken.value
            )
        )

    private fun requestTokens(parameters: Parameters): CompletableFuture<RefreshAndAccessTokens> {
        val future = CompletableFuture<RefreshAndAccessTokens>()
        val requestTimestamp = Instant.now()
        with(config) {
            fuel.post(instance.accessTokenUrl, parameters)
                .authentication()
                .basic(consumer.id, consumer.secret)
                .responseObject<OAuthTokensBean>(gson) { _, _, result ->
                    result.fold(
                        success = { future.complete(it.toOAuthTokens(requestTimestamp)) },
                        failure = { future.completeExceptionally(it.exception) }
                    )
                }
        }
        return future
    }
}

private val BitbucketCloud.accessTokenUrl
    get() = baseUrl.resolve("/site/oauth2/access_token").toString()

private data class OAuthTokensBean(
    val accessToken: String,
    val expiresIn: Long,
    val refreshToken: String
) {
    /**
     * Bitbucket returns relative expiration time, and to stay safe it's safer
     * to use time of the request when converting it to an absolute timestamp.
     */
    fun toOAuthTokens(requestTimestamp: Instant): RefreshAndAccessTokens =
        RefreshAndAccessTokens(
            RefreshToken(refreshToken),
            AccessToken(accessToken, requestTimestamp.plusSeconds(expiresIn))
        )
}
