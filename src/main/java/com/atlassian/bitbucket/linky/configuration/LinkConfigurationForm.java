package com.atlassian.bitbucket.linky.configuration;

import com.atlassian.bitbucket.linky.discovery.BitbucketCloudProbe;
import com.atlassian.bitbucket.linky.discovery.ManualRepositoryLinker;
import com.atlassian.bitbucket.linky.discovery.RemoteUrl;
import com.atlassian.bitbucket.linky.repository.RepositoryExtensionsKt;
import com.atlassian.bitbucket.linky.rest.cloud.BitbucketCloud;
import com.atlassian.bitbucket.linky.rest.server.BitbucketServer;
import com.intellij.dvcs.repo.Repository;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.options.Configurable;
import com.intellij.openapi.ui.ComboBox;
import com.intellij.openapi.util.text.StringUtil;
import com.intellij.openapi.vfs.VfsUtil;
import com.intellij.ui.CollectionComboBoxModel;
import com.intellij.ui.ListCellRendererWrapper;
import com.intellij.ui.components.JBTextField;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class LinkConfigurationForm implements Configurable {
    private ManualRepositoryLinker repositoryLinker;
    private JPanel mainPanel;
    private ComboBox<RemoteUrl> remoteComboBox;
    private JBTextField repoUrlTextField;
    private JTextField pathTextField;

    LinkConfigurationForm(Repository repository) {
        this.repositoryLinker = ServiceManager.getService(ManualRepositoryLinker.class);

        String relativePath = VfsUtil.getRelativePath(repository.getRoot(), repository.getProject().getBaseDir());
        if (StringUtils.isBlank(relativePath)) {
            relativePath = "<Project Root>";
        }
        pathTextField.setText(relativePath);

        List<RemoteUrl> remoteUrls = RepositoryExtensionsKt.getRemoteUrls(repository);
        remoteComboBox.setModel(new CollectionComboBoxModel<>(remoteUrls));
        if (remoteUrls.size() <= 1) {
            remoteComboBox.setEnabled(false);
        }
    }

    @Override
    public void disposeUIResources() {
    }

    @Override
    @NotNull
    public JComponent createComponent() {
        return mainPanel;
    }

    @Nls
    @Override
    public String getDisplayName() {
        return "Bitbucket Repository Link Configuration";
    }

    @Nullable
    @Override
    public String getHelpTopic() {
        return null;
    }

    @Override
    public boolean isModified() {
        return getRepositoryUrl()
                .map(url -> getBitbucketCloud(url).isPresent() || getBitbucketServer(url).isPresent())
                .orElse(false);
    }

    @Override
    public void reset() {
        if (remoteComboBox.getItemCount() > 0) {
            remoteComboBox.setSelectedIndex(0);
        }
        repoUrlTextField.setText("");
    }

    @Override
    public void apply() {
        Optional.ofNullable((RemoteUrl) remoteComboBox.getSelectedItem())
                .ifPresent(remoteUrl ->
                        getRepositoryUrl()
                                .ifPresent(url -> {
                                    Optional<BitbucketCloud> maybeCloud = getBitbucketCloud(url);
                                    if (maybeCloud.isPresent()) {
                                        maybeCloud.ifPresent(cloud -> repositoryLinker.register(remoteUrl, cloud));
                                    } else {
                                        Optional<BitbucketServer> maybeServer = getBitbucketServer(url);
                                        maybeServer.ifPresent(server -> repositoryLinker.register(remoteUrl, server));
                                    }
                                }));
    }

    private Optional<URI> getRepositoryUrl() {
        try {
            return Optional.ofNullable(StringUtil.nullize(repoUrlTextField.getText()))
                    .map(URI::create);
        } catch (IllegalArgumentException ignored) {
            return Optional.empty();
        }
    }

    private Optional<BitbucketCloud> getBitbucketCloud(URI repoUrl) {
        return Optional.ofNullable(repoUrl.getHost())
                .map(BitbucketCloudProbe.INSTANCE::probeBitbucketCloud);
    }

    private Optional<BitbucketServer> getBitbucketServer(URI repoUrl) {
        List<String> pathSegments = Arrays.asList(repoUrl.getPath().split("/"));
        // BBS URL path should be .../projects/KEY/repos/SLUG/...
        int projectsIndex = pathSegments.lastIndexOf("projects");
        int reposIndex = pathSegments.lastIndexOf("repos");
        if (reposIndex == projectsIndex + 2) {
            // TODO verify this
            URI baseUrl = repoUrl.resolve("/" + String.join("/", pathSegments.subList(0, projectsIndex)) + "/");
            return Optional.of(new BitbucketServer(baseUrl));
        } else {
            return Optional.empty();
        }
    }

    private void createUIComponents() {
        remoteComboBox = new ComboBox<>();
        remoteComboBox.setRenderer(new ListCellRendererWrapper<RemoteUrl>() {
            @Override
            public void customize(JList list, RemoteUrl remoteUrl, int index, boolean selected, boolean hasFocus) {
                StringBuilder sb = new StringBuilder();
                sb.append(remoteUrl.getScheme()).append("://").append(remoteUrl.getHostname());
                if (remoteUrl.getPort() > 0) {
                    sb.append(":").append(remoteUrl.getPort());
                }
                sb.append(remoteUrl.getPath());
                setText(sb.toString());
            }
        });

        repoUrlTextField = new JBTextField();
        repoUrlTextField.getEmptyText().setText("Paste a Bitbucket URL for this repository");
    }
}
