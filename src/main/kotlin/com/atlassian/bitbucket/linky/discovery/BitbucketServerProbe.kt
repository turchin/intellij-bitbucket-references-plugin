package com.atlassian.bitbucket.linky.discovery

import com.atlassian.bitbucket.linky.UriScheme
import com.atlassian.bitbucket.linky.UriScheme.GIT
import com.atlassian.bitbucket.linky.UriScheme.SSH
import com.atlassian.bitbucket.linky.logger
import com.atlassian.bitbucket.linky.rest.BadResponseCodeException
import com.atlassian.bitbucket.linky.rest.MalformedResponseException
import com.atlassian.bitbucket.linky.rest.configureFuelHttpClient
import com.atlassian.bitbucket.linky.rest.onBitbucketError
import com.atlassian.bitbucket.linky.rest.server.BitbucketServer
import com.atlassian.bitbucket.linky.rest.server.BitbucketServerApiImpl
import com.atlassian.bitbucket.linky.utils.anyMatch
import com.atlassian.bitbucket.linky.utils.appendTrailSlash
import io.mikael.urlbuilder.UrlBuilder
import java.io.IOException
import java.net.URI
import java.time.Duration
import java.time.Instant
import java.util.concurrent.CompletableFuture
import javax.net.ssl.SSLException

private val log = logger()
private val probeRetryTimeout = Duration.ofHours(1)

object BitbucketServerProbe {

    // URLs to never retry probing. It is not persisted because
    // we can't be sure things don't change in the outer world.
    private val blackList = mutableSetOf<URI>()
    private val probeTimestamps = mutableMapOf<URI, Instant>()

    fun probeBitbucketServer(
        detectedScheme: UriScheme,
        detectedHost: String,
        detectedPort: Int,
        detectedApplicationPath: String
    ): CompletableFuture<BitbucketServer?> {
        val appPath = if (detectedApplicationPath.isEmpty()) "/" else detectedApplicationPath
        return candidates(detectedScheme, detectedHost, detectedPort, appPath)
            .map { probe(it) }
            .anyMatch { it != null }
    }

    private fun candidates(
        detectedScheme: UriScheme,
        detectedHost: String,
        detectedPort: Int,
        detectedPath: String
    ): List<URI> {
        val candidateSchemes = when (detectedScheme) {
            SSH, GIT -> listOf(UriScheme.HTTPS, UriScheme.HTTP)
            else -> listOf(detectedScheme)
        }

        return candidateSchemes.flatMap { scheme ->
            listOf(-1, detectedPort)
                .distinct()
                .map { port ->
                    UrlBuilder.empty()
                        .withScheme(scheme.presentation)
                        .withHost(detectedHost)
                        .withPort(if (port > 0) port else null)
                        .withPath(detectedPath.appendTrailSlash())
                        .toUri()
                }
        }
    }

    private fun probe(url: URI): CompletableFuture<BitbucketServer?> =
        when {
            blackList.contains(url) || triedRecently(url) -> CompletableFuture.completedFuture(null)
            else -> {
                log.debug("Probing for Bitbucket Server at '$url")
                val server = BitbucketServer(url)
                probeTimestamps[url] = Instant.now()
                val client = BitbucketServerApiImpl(
                    createFuel = { configureFuelHttpClient(server.baseUrl) }
                ) {
                    instance = server
                }
                client.testConnectivity().unauthenticatedResource()
                    .thenApply { server }
                    .onBitbucketError { error ->
                        val errorMessage = "No Bitbucket Server found at '$url'"
                        when {
                            error is MalformedResponseException || error.cause is SSLException ->
                                blackList.add(url).also { log.debug(errorMessage, error) }
                            error is BadResponseCodeException || error.cause is IOException ->
                                log.debug(errorMessage, error)
                            error.cause == null ->
                                log.error("[unexpected null] $errorMessage", error)
                            else ->
                                log.error("[unexpected case] $errorMessage", error)
                        }
                    }.exceptionally { null }
            }
        }

    private fun triedRecently(url: URI) =
        probeTimestamps.getOrDefault(url, Instant.ofEpochMilli(0))
            .isAfter(Instant.now().minus(probeRetryTimeout))
}
