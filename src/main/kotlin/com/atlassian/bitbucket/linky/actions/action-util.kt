package com.atlassian.bitbucket.linky.actions

import com.atlassian.bitbucket.linky.BitbucketRepository
import com.atlassian.bitbucket.linky.clipboard.TransferableLink
import com.atlassian.bitbucket.linky.stats.UsageLog
import com.atlassian.bitbucket.linky.stats.cloudEventAttributes
import com.atlassian.bitbucket.linky.stats.serverEventAttributes
import com.intellij.ide.BrowserUtil
import com.intellij.ide.DataManager
import com.intellij.openapi.actionSystem.*
import com.intellij.openapi.application.ApplicationManager
import com.intellij.openapi.components.service
import com.intellij.openapi.ide.CopyPasteManager
import com.intellij.openapi.progress.ProgressIndicator
import com.intellij.openapi.progress.util.ProgressIndicatorUtils
import com.intellij.openapi.progress.util.ReadTask
import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.MessageType
import com.intellij.openapi.ui.popup.Balloon
import com.intellij.openapi.ui.popup.JBPopupFactory
import com.intellij.openapi.wm.WindowManager
import com.intellij.ui.awt.RelativePoint
import com.intellij.util.TimeoutUtil
import java.net.URI
import java.time.Duration

data class Link(val uri: URI, val text: String)

fun copyToClipboard(project: Project, link: Link, warn: Boolean = false) {
    CopyPasteManager.getInstance().setContents(TransferableLink(link.uri, link.text))
    val messageType = if (warn) MessageType.WARNING else MessageType.INFO
    showStatusBarPopupMessage(project, messageType, "Link to <b>${link.text}</b> has been copied")
}

fun logActionUsage(
    name: String,
    place: String,
    repository: BitbucketRepository,
    attributes: Map<String, Any> = emptyMap()
) {
    val bitbucket = when (repository) {
        is BitbucketRepository.Cloud -> cloudEventAttributes
        is BitbucketRepository.Server -> serverEventAttributes
    }
    service<UsageLog>().event(name, place, attributes + bitbucket)
}

fun openUriInBrowser(project: Project, link: Link, warn: Boolean = false) {
    val messageType = if (warn) MessageType.WARNING else MessageType.INFO
    showStatusBarPopupMessage(project, messageType, "<b>${link.text}</b> is now being opened in browser")

    val delay = if (warn) Duration.ofSeconds(2) else Duration.ZERO
    delayAction(delay) { BrowserUtil.browse(link.uri) }
}

fun showStatusBarPopupMessage(project: Project, messageType: MessageType, message: String) {
    ApplicationManager.getApplication().invokeLater {
        WindowManager.getInstance().getStatusBar(project)?.let { statusBar ->
            JBPopupFactory.getInstance()
                .createHtmlTextBalloonBuilder(message, messageType, null)
                .setFadeoutTime(5000)
                .createBalloon()
                .show(
                    RelativePoint.getCenterOf(statusBar.component),
                    Balloon.Position.atRight
                )
        }
    }
}

fun showLogInFileManager() = triggerAction("ShowLog")

fun triggerAction(action: AnAction) {
    DataManager.getInstance().dataContextFromFocusAsync
        .onSuccess { ctx ->
            val actionManager = ActionManager.getInstance()
            val actionEvent = AnActionEvent(null, ctx, ActionPlaces.UNKNOWN, Presentation(), actionManager, 0)
            action.actionPerformed(actionEvent)
        }
}

private fun delayAction(delay: Duration, action: () -> Unit) {
    ProgressIndicatorUtils.scheduleWithWriteActionPriority(object : ReadTask() {
        override fun performInReadAction(indicator: ProgressIndicator): Continuation? {
            TimeoutUtil.sleep(delay.toMillis())
            return Continuation(action)
        }

        override fun onCanceled(indicator: ProgressIndicator) {
            ProgressIndicatorUtils.scheduleWithWriteActionPriority(this)
        }
    })
}

private fun triggerAction(actionId: String) {
    ActionManager.getInstance().getAction(actionId)?.let { triggerAction(it) }
}
