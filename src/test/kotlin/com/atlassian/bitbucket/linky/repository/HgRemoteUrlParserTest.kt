package com.atlassian.bitbucket.linky.repository

import assertk.all
import assertk.assertThat
import assertk.assertions.isEqualTo
import assertk.assertions.isNotNull
import assertk.assertions.isNull
import assertk.assertions.prop
import com.atlassian.bitbucket.linky.UriScheme
import com.atlassian.bitbucket.linky.UriScheme.*
import com.atlassian.bitbucket.linky.discovery.RemoteUrl
import org.junit.jupiter.api.Test

internal class HgRemoteUrlParserTest {
    @Test
    fun `test Cloud remote URL`() {
        assertUrlParsed(
            "ssh://hg@bitbucket.org/atlas/test",
            SSH, "bitbucket.org", -1, "/atlas/test"
        )
        assertUrlParsed(
            "https://dpenkin@bitbucket.org/atlas/test",
            HTTPS, "bitbucket.org", -1, "/atlas/test"
        )
    }

    @Test
    fun `test parse other remote URLs`() {
        assertUrlParsed(
            "http://example",
            HTTP, "example", -1, "/"
        )
        assertUrlParsed(
            "http://example.com",
            HTTP, "example.com", -1, "/"
        )
        assertUrlParsed(
            "http://example.com/",
            HTTP, "example.com", -1, "/"
        )
        assertUrlParsed(
            "http://example.com:239",
            HTTP, "example.com", 239, "/"
        )
        assertUrlParsed(
            "http://example.com:239/",
            HTTP, "example.com", 239, "/"
        )
        assertUrlParsed(
            "http://example.com:239/hello/world/",
            HTTP, "example.com", 239, "/hello/world"
        )
    }

    @Test
    fun `test parse other schemes returns null`() {
        assertThat(HgRemoteUrlParser.parseRemoteUrl("file://whatever")).isNull()
        assertThat(HgRemoteUrlParser.parseRemoteUrl("example")).isNull()
    }

    private fun assertUrlParsed(
        urlToParse: String,
        expectedScheme: UriScheme,
        expectedHostname: String,
        expectedPort: Int,
        expectedPath: String
    ) {
        val url = HgRemoteUrlParser.parseRemoteUrl(urlToParse)
        assertThat(url).isNotNull().all {
            prop(RemoteUrl::scheme).isEqualTo(expectedScheme)
            prop(RemoteUrl::hostname).isEqualTo(expectedHostname)
            prop(RemoteUrl::port).isEqualTo(expectedPort)
            prop(RemoteUrl::path).isEqualTo(expectedPath)
        }
    }
}
