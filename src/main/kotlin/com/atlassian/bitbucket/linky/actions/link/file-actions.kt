package com.atlassian.bitbucket.linky.actions.link

import com.atlassian.bitbucket.linky.BitbucketRepository
import com.atlassian.bitbucket.linky.LinkyFile
import com.atlassian.bitbucket.linky.actions.*
import com.atlassian.bitbucket.linky.icons.BitbucketLinkyIcons.Actions.FileCopyUrl
import com.atlassian.bitbucket.linky.icons.BitbucketLinkyIcons.Actions.FileOpen
import com.atlassian.bitbucket.linky.repository.hasRevisionBeenPushed
import com.atlassian.bitbucket.linky.selection.LinesSelection
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.CommonDataKeys
import com.intellij.openapi.editor.Document
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.project.DumbAwareAction
import com.intellij.openapi.project.Project
import org.apache.commons.lang.StringEscapeUtils.escapeHtml
import javax.swing.Icon

private const val LINK_TEXT_MAX_LENGTH = 100

abstract class AbstractFileSourceViewAction(text: String, description: String, icon: Icon) :
    DumbAwareAction(text, description, icon) {

    override fun actionPerformed(event: AnActionEvent) {
        val ctx = event.linkyActionContext()
        if (ctx is LinkyFileActionContext) {
            val editor = CommonDataKeys.EDITOR.getData(event.dataContext)
            val lineSelections = editor?.let { getLineSelections(it) } ?: listOf()
            val linkyFile = ctx.linkyFile
            val linky = ctx.getLinky() ?: return

            val url = linky.getSourceViewUri(linkyFile, lineSelections)
            val revisionNotPushed = !linkyFile.repository.hasRevisionBeenPushed(linkyFile.revision)
            if (revisionNotPushed) {
                showLinkToLocalRevisionError(ctx.project, linkyFile.repository)
            }
            val link = Link(url, getLinkText(linkyFile, editor))
            val actionStats = actionStats(event.place, linky.getRepository(), editor, lineSelections)
            actionPerformed(ctx.project, link, revisionNotPushed, actionStats)
        }
    }

    override fun update(event: AnActionEvent) {
        val actionContext = event.linkyActionContext()
        event.presentation.isEnabledAndVisible =
            actionContext is LinkyFileActionContext && actionContext.getLinky() != null
    }

    internal abstract fun actionPerformed(project: Project, link: Link, warn: Boolean, stats: FileActionStats)

    private fun getLineSelections(editor: Editor): List<LinesSelection> =
        with(editor.selectionModel) {
            blockSelectionStarts.zip(blockSelectionEnds)
                .mapNotNull { (start, end) -> getLineSelection(editor.document, start, end) }
        }

    private fun getLineSelection(
        document: Document,
        selectionStartOffset: Int,
        selectionEndOffset: Int
    ): LinesSelection? {
        var startLineNumber = document.getLineNumber(selectionStartOffset)
        val startLineEndOffset = document.getLineEndOffset(startLineNumber)
        // exclude first line if selection starts from its very end
        if (selectionStartOffset == startLineEndOffset) {
            startLineNumber++
        }

        var endLineNumber = document.getLineNumber(selectionEndOffset)
        val endLineStartOffset = document.getLineStartOffset(endLineNumber)
        // exclude last line if selection ends on its very beginning
        if (selectionEndOffset == endLineStartOffset) {
            endLineNumber--
        }

        return if (startLineNumber <= endLineNumber) LinesSelection(startLineNumber + 1, endLineNumber + 1) else null
    }

    private fun getLinkText(linkyFile: LinkyFile, editor: Editor?): String {
        if (editor?.caretModel?.caretCount == 1) {
            val text = editor.selectionModel.selectedText?.trim() ?: ""
            if (text.isNotBlank() &&
                text.length <= LINK_TEXT_MAX_LENGTH &&
                !text.contains(Regex("[^ -~]+"))
            ) {
                return escapeHtml(text)
            }
        }
        return linkyFile.name
    }

    private fun actionStats(
        place: String,
        repository: BitbucketRepository,
        editor: Editor?,
        linesSelections: List<LinesSelection>
    ): FileActionStats {
        val multipleLines = linesSelections.size > 1 ||
                linesSelections.firstOrNull()?.let { it.lastLineNumber > it.firstLineNumber } ?: false
        val multipleCarets = editor?.caretModel?.caretCount ?: 0 > 1
        return FileActionStats(place, repository, editor != null, multipleLines, multipleCarets)
    }
}

internal data class FileActionStats(
    val place: String,
    val repository: BitbucketRepository,
    val editor: Boolean,
    val multipleLines: Boolean,
    val multipleCarets: Boolean
) {
    fun attributesMap() = mapOf(
        "editor" to editor,
        "multipleLines" to multipleLines,
        "multipleCarets" to multipleCarets
    )
}

class CopyFileUrlInBitbucketAction : AbstractFileSourceViewAction(
    "Copy Bitbucket link", "Copy URL for Bitbucket Source view of the current file", FileCopyUrl
) {
    override fun actionPerformed(project: Project, link: Link, warn: Boolean, stats: FileActionStats) {
        copyToClipboard(project, link, warn)
        logActionUsage("CopyLinkToFile", stats.place, stats.repository, stats.attributesMap())
    }
}

class OpenFileInBitbucketAction : AbstractFileSourceViewAction(
    "Open in Bitbucket", "Open Bitbucket Source view of the current file", FileOpen
) {
    override fun actionPerformed(project: Project, link: Link, warn: Boolean, stats: FileActionStats) {
        openUriInBrowser(project, link, warn)
        logActionUsage("OpenFile", stats.place, stats.repository, stats.attributesMap())
    }
}
