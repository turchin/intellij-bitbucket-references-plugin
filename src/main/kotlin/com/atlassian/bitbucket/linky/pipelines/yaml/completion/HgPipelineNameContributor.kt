package com.atlassian.bitbucket.linky.pipelines.yaml.completion

import com.atlassian.bitbucket.linky.pipelines.yaml.pattern.inPipelinesFilePattern
import com.atlassian.bitbucket.linky.pipelines.yaml.pattern.withinYamlObject
import com.atlassian.bitbucket.linky.pipelines.yaml.vcs.repositoryForFile
import com.intellij.codeInsight.completion.*
import com.intellij.codeInsight.lookup.LookupElementBuilder
import com.intellij.icons.AllIcons
import com.intellij.patterns.PlatformPatterns.psiElement
import com.intellij.util.ProcessingContext
import org.zmlx.hg4idea.repo.HgRepository

class HgPipelineNameContributor : CompletionContributor() {
    init {
        extend(CompletionType.BASIC, bookmarkNameCompletionPattern, HgBookmarkNameCompletionProvider)
        extend(CompletionType.BASIC, branchNameCompletionPattern, HgBranchNameCompletionProvider)
        extend(CompletionType.BASIC, tagNameCompletionPattern, HgTagNameCompletionProvider)
    }
}

private val bookmarkNameCompletionPattern =
    psiElement()
        .and(inPipelinesFilePattern)
        .and(withinYamlObject("bookmarks"))

private val branchNameCompletionPattern =
    psiElement()
        .and(inPipelinesFilePattern)
        .andOr(
            withinYamlObject("branches"),
            withinYamlObject("pull-requests")
        )

private val tagNameCompletionPattern =
    psiElement()
        .and(inPipelinesFilePattern)
        .and(withinYamlObject("tags"))

abstract class HgCompletionProvider : CompletionProvider<CompletionParameters>() {
    override fun addCompletions(
        parameters: CompletionParameters,
        context: ProcessingContext,
        result: CompletionResultSet
    ) {
        val repository = repositoryForFile(parameters.originalFile) as? HgRepository ?: return
        addCompletions(repository, result)
    }

    abstract fun addCompletions(repository: HgRepository, result: CompletionResultSet)
}

object HgBranchNameCompletionProvider : HgCompletionProvider() {
    override fun addCompletions(
        repository: HgRepository,
        result: CompletionResultSet
    ) {
        val branches = repository.branches.keys
        result.addAllElements(
            branches.map {
                LookupElementBuilder.create(it)
                    .withIcon(AllIcons.Vcs.Branch)
            }
        )
    }
}

object HgTagNameCompletionProvider : HgCompletionProvider() {
    override fun addCompletions(
        repository: HgRepository,
        result: CompletionResultSet
    ) {
        val tags = repository.tags
        result.addAllElements(
            tags.map {
                LookupElementBuilder.create(it.name)
                    .withIcon(AllIcons.General.Balloon)
            }
        )
    }
}

object HgBookmarkNameCompletionProvider : HgCompletionProvider() {
    override fun addCompletions(
        repository: HgRepository,
        result: CompletionResultSet
    ) {
        val bookmarks = repository.bookmarks

        result.addAllElements(
            bookmarks.map {
                LookupElementBuilder.create(it.name)
                    .withIcon(AllIcons.Actions.Annotate)
            }
        )
    }
}
