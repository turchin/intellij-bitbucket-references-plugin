package com.atlassian.bitbucket.linky.actions

import com.atlassian.bitbucket.linky.icons.BitbucketLinkyIcons
import com.intellij.dvcs.push.ui.VcsPushDialog
import com.intellij.dvcs.repo.Repository
import com.intellij.notification.*
import com.intellij.openapi.project.Project
import com.intellij.openapi.vcs.changes.ui.ChangesViewContentManager
import com.intellij.openapi.wm.ToolWindowManager
import com.intellij.vcs.log.impl.VcsLogContentProvider
import javax.swing.event.HyperlinkEvent

val notificationGroup =
    NotificationGroup("Bitbucket Linky", NotificationDisplayType.BALLOON, true)
val stickyNotificationGroup =
    NotificationGroup("Bitbucket Linky", NotificationDisplayType.STICKY_BALLOON, true)

fun showLinkToLocalRevisionError(project: Project, repository: Repository) {
    stickyNotificationGroup.createNotification(
        "Bitbucket Linky: link to a local commit",
        "<p>You created a link for a commit that exists only in your local repository and has not been pushed to the remote repository.</p>" +
                "<p>You'll get <b>404 Not Found</b> error when you open this link in the browser.</p>" +
                "<p><br></p>" +
                "<a href=\"#push\">Push changes to the remote repository</a>" +
                "<br>" +
                "<a href=\"#vcslog\">Create a link for another commit in the VCS log</a>",
        NotificationType.WARNING,
        object : NotificationListener.Adapter() {
            override fun hyperlinkActivated(notification: Notification, event: HyperlinkEvent) {
                when (event.description) {
                    "#push" -> VcsPushDialog(project, listOf(repository), repository).show()
                    "#vcslog" -> {
                        val window = ToolWindowManager.getInstance(project)
                            .getToolWindow(ChangesViewContentManager.TOOLWINDOW_ID)
                        val contentManager = window.contentManager
                        for (content in contentManager.contents) {
                            if (VcsLogContentProvider.TAB_NAME == content.displayName) {
                                contentManager.setSelectedContent(content)
                                break
                            }
                        }
                        if (!window.isVisible) {
                            window.activate(null, true)
                        }
                    }
                }
            }
        })
        .setIcon(BitbucketLinkyIcons.Bitbucket)
        .setImportant(true)
        .notify(project)
}
