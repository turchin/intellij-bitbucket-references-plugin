package com.atlassian.bitbucket.linky.pipelines.yaml.inspection

import com.atlassian.bitbucket.linky.LinkyBundle.message
import com.atlassian.bitbucket.linky.pipelines.yaml.pattern.cacheReferencesPattern
import com.atlassian.bitbucket.linky.pipelines.yaml.pattern.serviceReferencesPattern
import com.intellij.codeInspection.*
import com.intellij.openapi.project.Project
import com.intellij.psi.PsiElementVisitor
import org.jetbrains.yaml.psi.YAMLScalar
import org.jetbrains.yaml.psi.YAMLSequence
import org.jetbrains.yaml.psi.YAMLSequenceItem
import org.jetbrains.yaml.psi.YamlPsiElementVisitor

class DuplicateDefinitionReferenceInspection : LocalInspectionTool() {

    override fun buildVisitor(
        holder: ProblemsHolder,
        isOnTheFly: Boolean
    ): PsiElementVisitor = object : YamlPsiElementVisitor() {
        override fun visitSequence(sequence: YAMLSequence) {
            if (cacheReferencesPattern.accepts(sequence)) {
                inspectDuplicates(holder, sequence) { cacheName ->
                    ProblemContext(
                        message("inspections.duplicate.step.cache.definition.reference.problem.text", cacheName),
                        ProblemHighlightType.WEAK_WARNING,
                        removeDuplicateCacheDefinitionReferenceQuickFix(cacheName)
                    )
                }
            } else if (serviceReferencesPattern.accepts(sequence)) {
                inspectDuplicates(holder, sequence) { serviceName ->
                    ProblemContext(
                        message("inspections.duplicate.step.service.definition.reference.problem.text", serviceName),
                        ProblemHighlightType.GENERIC_ERROR_OR_WARNING,
                        removeDuplicateServiceDefinitionReferenceQuickFix(serviceName)
                    )
                }
            }
        }
    }

    private fun inspectDuplicates(
        holder: ProblemsHolder,
        sequence: YAMLSequence,
        problemContextProvider: (String) -> ProblemContext
    ) {
        sequence.items
            .mapNotNull { it.value as? YAMLScalar }
            .groupBy { it.textValue }
            .filter { (_, psis) -> psis.size > 1 }
            .forEach { (refName, psis) ->
                for (psi in psis) {
                    val problemContext = problemContextProvider(refName)
                    holder.registerProblem(
                        psi,
                        problemContext.problemText,
                        problemContext.level,
                        problemContext.quickFix
                    )
                }
            }

    }
}

private class ProblemContext(
    val problemText: String,
    val level: ProblemHighlightType,
    val quickFix: LocalQuickFix
)

private fun removeDuplicateCacheDefinitionReferenceQuickFix(cacheName: String) =
    RemoveDuplicateDefinitionReferenceQuickFix(
        message("inspections.duplicate.step.cache.definition.reference.problem.fix.family.name"),
        message("inspections.duplicate.step.cache.definition.reference.problem.fix.name", cacheName)
    )

private fun removeDuplicateServiceDefinitionReferenceQuickFix(serviceName: String) =
    RemoveDuplicateDefinitionReferenceQuickFix(
        message("inspections.duplicate.step.service.definition.reference.problem.fix.family.name"),
        message("inspections.duplicate.step.service.definition.reference.problem.fix.name", serviceName)
    )

private class RemoveDuplicateDefinitionReferenceQuickFix(
    private val familyName: String,
    private val name: String
) : LocalQuickFix {
    override fun getName() = name

    override fun getFamilyName() = familyName

    override fun applyFix(project: Project, descriptor: ProblemDescriptor) {
        val reference = descriptor.psiElement as? YAMLScalar ?: return
        val sequenceItem = reference.parent as? YAMLSequenceItem ?: return

        sequenceItem.deleteAndPositionCaret()
    }
}
