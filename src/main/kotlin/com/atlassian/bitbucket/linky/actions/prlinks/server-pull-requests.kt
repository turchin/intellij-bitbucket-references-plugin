package com.atlassian.bitbucket.linky.actions.prlinks

import com.atlassian.bitbucket.linky.BitbucketRepository
import com.atlassian.bitbucket.linky.LinkyBundle.message
import com.atlassian.bitbucket.linky.Revision
import com.atlassian.bitbucket.linky.rest.BitbucketRestClientProvider
import com.atlassian.bitbucket.linky.rest.pagination.fetchRemaining
import com.atlassian.bitbucket.linky.rest.server.RepositoryId
import com.intellij.openapi.components.service
import java.util.concurrent.CompletableFuture

fun fetchRelatedPullRequests(
    repository: BitbucketRepository.Server,
    revision: Revision
): CompletableFuture<List<RelatedPullRequest>?> {
    val project = repository.repository.project
    val server = repository.hosting

    val client = project.service<BitbucketRestClientProvider>()
        .bitbucketServerRestClient(
            server, message("action.list.related.pull.requests.authentication.required.label")
        )

    return client.thenCompose {
        it?.let { api ->
            val pullRequests = mutableListOf<RelatedPullRequest>()
            val repoId = RepositoryId(repository.projectKey, repository.slug)

            api.repository(repoId)
                .commit(revision)
                .relatedPullRequests()
                .fetchRemaining(api) { items -> pullRequests.addAll(items) }
                .thenApply { pullRequests.toList() }
        } ?: CompletableFuture.completedFuture<List<RelatedPullRequest>?>(null)
    }
}
