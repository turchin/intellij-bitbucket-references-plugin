package com.atlassian.bitbucket.linky.discovery

import com.atlassian.bitbucket.linky.logger
import com.atlassian.bitbucket.linky.rest.cloud.BitbucketCloud
import com.atlassian.bitbucket.linky.rest.cloud.BitbucketCloud.Production
import com.atlassian.bitbucket.linky.rest.cloud.BitbucketCloud.Staging

private val log = logger()

object BitbucketCloudProbe {
    fun probeBitbucketCloud(detectedHost: String): BitbucketCloud? =
        when (detectedHost) {
            "bitbucket.org" -> Production
            "altssh.bitbucket.org" -> Production
            "staging.bb-inf.net" -> Staging
            else -> null.also { log.debug("Candidate host doesn't look like Bitbucket Cloud '$detectedHost'") }
        }
}
