package com.atlassian.bitbucket.linky.rest.cloud.oauth

import com.atlassian.bitbucket.linky.rest.auth.OAuthConsumer
import com.atlassian.bitbucket.linky.rest.auth.OAuthTokens.RefreshAndAccessTokens
import com.atlassian.bitbucket.linky.rest.auth.RefreshToken
import com.atlassian.bitbucket.linky.rest.cloud.BitbucketCloud
import java.util.concurrent.CompletableFuture

interface BitbucketCloudOAuthApi {
    fun requestOAuthTokens(authorizationCode: AuthorizationCode): CompletableFuture<RefreshAndAccessTokens>

    fun refreshOAuthTokens(refreshToken: RefreshToken): CompletableFuture<RefreshAndAccessTokens>
}

class BitbucketCloudOAuthApiConfig {
    var instance: BitbucketCloud = BitbucketCloud.Production
    lateinit var consumer: OAuthConsumer
}

inline class AuthorizationCode(val value: String)
