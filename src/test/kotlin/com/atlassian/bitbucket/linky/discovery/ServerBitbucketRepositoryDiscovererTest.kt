//package com.atlassian.bitbucket.linky.discovery
//
//import assertk.all
//import assertk.assertThat
//import assertk.assertions.*
//import com.atlassian.bitbucket.linky.BitbucketRepository
//import com.atlassian.bitbucket.linky.UriScheme.HTTPS
//import com.atlassian.bitbucket.linky.UriScheme.SSH
//import com.atlassian.bitbucket.linky.hosting.BitbucketServerRegistry
//import com.atlassian.bitbucket.server.rest.BitbucketServer
//import com.intellij.dvcs.repo.Repository
//import io.mockk.*
//import kotlinx.coroutines.runBlocking
//import org.junit.jupiter.api.BeforeEach
//import org.junit.jupiter.api.Test
//import java.net.URI
//
//private val TEST_URI = URI.create("http://example.com")
//private val TEST_SERVER = BitbucketServer(TEST_URI)
//
//internal class ServerBitbucketRepositoryDiscovererTest {
//    private val registry = mockk<BitbucketServerRegistry>(relaxUnitFun = true)
//    private val repository = mockk<Repository>()
//    private val probe = mockk<BitbucketServerProbe>()
//
//    private val discoverer = ServerBitbucketRepositoryDiscoverer(registry, probe)
//
//    private val remoteUrl = RemoteUrl(SSH, "stash.atlassian.com", -1, "/bitbucket/test")
//
//    @BeforeEach
//    internal fun setUp() {
//        every { registry.lookup(any(), any(), any(), any()) } returns null
//        coEvery { probe.probeBitbucketServer(any(), any(), any(), any()) } returns TEST_SERVER
//    }
//
//    @Test
//    fun `test repository discovery`() {
//        val repo = runBlocking { discoverer.discover(repository, remoteUrl, true) }
//
//        assertThat(repo).isNotNull().isInstanceOf(BitbucketRepository.Server::class).all {
//            prop(BitbucketRepository.Server::repository).isEqualTo(repository)
//            prop(BitbucketRepository.Server::hosting).isEqualTo(TEST_SERVER)
//            prop(BitbucketRepository.Server::projectKey).isEqualTo("bitbucket")
//            prop(BitbucketRepository.Server::slug).isEqualTo("test")
//        }
//    }
//
//    @Test
//    fun `should not probe if hosting found in registry`() {
//        every { registry.lookup(any(), any(), any(), any()) } returns TEST_SERVER
//
//        runBlocking { discoverer.discover(repository, remoteUrl, true) }
//
//        verify { registry.lookup(SSH, "stash.atlassian.com", -1, "/") }
//        verify(exactly = 0) { registry.add(any(), any(), any(), any(), any()) }
//        verify { probe wasNot called }
//    }
//
//    @Test
//    fun `should not probe if not active discovery`() {
//        val repository = runBlocking { discoverer.discover(repository, remoteUrl, false) }
//
//        assertThat(repository).isNull()
//
//        verify { registry.lookup(SSH, "stash.atlassian.com", -1, "/") }
//        verify(exactly = 0) { registry.add(any(), any(), any(), any(), any()) }
//        verify { probe wasNot called }
//    }
//
//    @Test
//    fun `should not register if probe found nothing`() {
//        coEvery { probe.probeBitbucketServer(any(), any(), any(), any()) } returns null
//
//        val repository = runBlocking { discoverer.discover(repository, remoteUrl, true) }
//
//        assertThat(repository).isNull()
//
//        verify { registry.lookup(SSH, "stash.atlassian.com", -1, "/") }
//        verify(exactly = 0) { registry.add(any(), any(), any(), any(), any()) }
//        coVerify { probe.probeBitbucketServer(SSH, "stash.atlassian.com", -1, "/") }
//    }
//
//    @Test
//    fun `should register if probe found Bitbucket`() {
//        runBlocking { discoverer.discover(repository, remoteUrl, true) }
//
//        verify { registry.lookup(SSH, "stash.atlassian.com", -1, "/") }
//        verify { registry.add(any(), any(), any(), any(), any()) }
//        coVerify { probe.probeBitbucketServer(SSH, "stash.atlassian.com", -1, "/") }
//    }
//
//    @Test
//    fun `should skip remotes if path unknown`() {
//        val discoverRepoForRemoteWithPath = { path: String ->
//            runBlocking {
//                discoverer.discover(repository, RemoteUrl(HTTPS, "example.com", -1, path), true)
//            }
//        }
//
//        assertThat(discoverRepoForRemoteWithPath("/three/segments/path")).isNull()
//        assertThat(discoverRepoForRemoteWithPath("/short_path")).isNull()
//        assertThat(discoverRepoForRemoteWithPath("/a//b")).isNull()
//        assertThat(discoverRepoForRemoteWithPath("///a")).isNull()
//        assertThat(discoverRepoForRemoteWithPath("//a")).isNull()
//        assertThat(discoverRepoForRemoteWithPath("/")).isNull()
//
//        verify { registry wasNot called }
//        verify { probe wasNot called }
//    }
//}
