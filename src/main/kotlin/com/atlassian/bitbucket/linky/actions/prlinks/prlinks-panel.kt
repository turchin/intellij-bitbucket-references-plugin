package com.atlassian.bitbucket.linky.actions.prlinks

import com.atlassian.bitbucket.linky.BitbucketRepository
import com.atlassian.bitbucket.linky.LINKY_ISSUES_URL
import com.atlassian.bitbucket.linky.Revision
import com.atlassian.bitbucket.linky.actions.*
import com.atlassian.bitbucket.linky.icons.BitbucketLinkyIcons.Actions.PullRequestCopyUrl
import com.atlassian.bitbucket.linky.icons.BitbucketLinkyIcons.Actions.PullRequestOpen
import com.atlassian.bitbucket.linky.logger
import com.atlassian.bitbucket.linky.rest.PullRequest
import com.atlassian.bitbucket.linky.stats.UsageLog
import com.atlassian.bitbucket.linky.stats.cloudEventAttributes
import com.intellij.ide.BrowserUtil
import com.intellij.openapi.actionSystem.*
import com.intellij.openapi.components.service
import com.intellij.openapi.project.DumbAwareAction
import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.PanelWithActionsAndCloseButton
import com.intellij.ui.JBSplitter
import com.intellij.ui.SimpleTextAttributes
import com.intellij.ui.components.JBScrollPane
import com.intellij.ui.content.ContentManager
import com.intellij.ui.table.JBTable
import org.commonmark.parser.Parser
import org.commonmark.renderer.html.HtmlRenderer
import java.awt.BorderLayout
import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import javax.swing.*
import javax.swing.table.AbstractTableModel
import javax.swing.table.DefaultTableCellRenderer
import javax.swing.table.TableColumn

typealias RelatedPullRequest = PullRequest

private val log = logger()

private const val ID_COLUMN = "ID"
private const val CREATED_COLUMN = "Created"
private const val STATE_COLUMN = "State"
private const val AUTHOR_COLUMN = "Author"
private const val TITLE_COLUMN = "Title"

private val dateFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy")
private val centeredCellRenderer = DefaultTableCellRenderer().apply { horizontalAlignment = JLabel.CENTER }

class CommitPullRequestsPanel(contentManager: ContentManager?) :
    PanelWithActionsAndCloseButton(contentManager, null) {

    private val prDetailsPane = JEditorPane("text/html", "")
    private val prDetailsPanel = JPanel(BorderLayout()).apply { add(JBScrollPane(prDetailsPane)) }

    private val prTableModel = PullRequestTableModel()
    private val prTable = JBTable(prTableModel).apply {
        showVerticalLines = false
        showHorizontalLines = false
        setSelectionMode(ListSelectionModel.SINGLE_SELECTION)
        selectionModel.addListSelectionListener {
            if (selectedRow > -1) {
                val pr = prTableModel.pullRequests[selectedRow]
                prDetailsPane.text = "<html><head>${prDetailsStyle()}</head><body>${pr.html()}</body></html>"
                prDetailsPane.caretPosition = 0
            } else {
                prDetailsPane.text = ""
            }
        }
        autoResizeMode = JTable.AUTO_RESIZE_LAST_COLUMN
        getColumn(ID_COLUMN).fixedWidthCentered(50)
        getColumn(CREATED_COLUMN).fixedWidthCentered(90)
        getColumn(STATE_COLUMN).fixedWidthCentered(90)
        getColumn(AUTHOR_COLUMN).fixedWidthCentered(180)
    }
    private val pullRequestProvider = {
        prTable.selectedRow
            .takeIf { it > -1 }
            ?.let { prTableModel.pullRequests.getOrNull(it) }
    }

    private val prTablePanel = JPanel(BorderLayout()).apply { add(JBScrollPane(prTable), BorderLayout.CENTER) }


    private val tableDetailsSplitter =
        JBSplitter("bitbucketlinky.prlinks.toolwindow.splitter", 0.7f)
            .apply {
                firstComponent = prTablePanel
                secondComponent = prDetailsPanel
            }

    private val mainPanel = JPanel(BorderLayout()).apply { add(tableDetailsSplitter, BorderLayout.CENTER) }

    private val openSelectedPullRequestAction = OpenSelectedPullRequestInBrowserAction(pullRequestProvider)

    private var actionGroup: DefaultActionGroup? = null

    init {
        init()
        // Removing help action added by superclass
        actionGroup?.apply { remove(ActionManager.getInstance().getAction(IdeActions.ACTION_CONTEXT_HELP)) }

        prTable.addMouseListener(object : MouseAdapter() {
            override fun mouseClicked(e: MouseEvent) {
                if (prTable.rowAtPoint(e.point) < 0) {
                    prTable.clearSelection()
                } else if (SwingUtilities.isLeftMouseButton(e) && e.clickCount == 2) {
                    triggerAction(openSelectedPullRequestAction)
                }
            }
        })
    }

    override fun addActionsTo(group: DefaultActionGroup?) {
        actionGroup = group

        actionGroup?.add(CopySelectedPullRequestUrlAction(pullRequestProvider))
        actionGroup?.add(openSelectedPullRequestAction)
    }

    override fun createCenterPanel() = mainPanel
    override fun dispose() = Unit

    fun showLoading() {
        prTableModel.pullRequests = listOf()
        prTable.emptyText.clear().appendText("Looking for related Pull Requests...")
        prTable.setPaintBusy(true)
    }

    fun showPullRequests(pullRequests: List<RelatedPullRequest>) {
        prTableModel.pullRequests = pullRequests
        prTable.emptyText.text = "No related Pull Requests"
        prTable.setPaintBusy(false)

        if (pullRequests.isNotEmpty()) {
            prTable.selectionModel.setSelectionInterval(0, 0)
        }
    }

    fun showPullRequestsAreBeingIndexed() {
        prTable.emptyText.clear()
            .appendText("Pull Requests are being indexed now, please try again after a while")
        prTable.setPaintBusy(false)
    }

    fun showPullRequestsNeedToBeIndexed(repository: BitbucketRepository.Cloud, commit: Revision) {
        prTable.emptyText.clear()
            .appendText("Enable lookup by clicking on \"Pull Requests\" link on ")
            .appendText("Bitbucket commit view", SimpleTextAttributes.LINK_ATTRIBUTES) {
                BrowserUtil.browse(repository.commit(commit))
            }
            .appendText(". You need to do this only once for ${repository.workspaceId} workspace.")
        prTable.setPaintBusy(false)
    }

    fun showAuthenticationRequiredError() {
        prTable.emptyText.clear()
            .appendText("Authentication required to find related Pull Requests")
        prTable.setPaintBusy(false)
    }

    fun showGenericError() {
        prTable.emptyText.clear()
            .appendText("Failed to lookup related Pull Requests, please ")
            .appendText("check the log", SimpleTextAttributes.LINK_ATTRIBUTES) { showLogInFileManager() }
            .appendText(" and ")
            .appendText("raise an issue", SimpleTextAttributes.LINK_ATTRIBUTES) { BrowserUtil.browse(LINKY_ISSUES_URL) }
        prTable.setPaintBusy(false)
    }

    fun showNetworkError() {
        prTable.emptyText.clear()
            .appendText("Bitbucket seems unreachable, check your network connection")
        prTable.setPaintBusy(false)
    }

    private class PullRequestTableModel : AbstractTableModel() {
        private val idColumn = 0
        private val createdDateColumn = 1
        private val stateColumn = 2
        private val authorColumn = 3
        private val titleColumn = 4

        internal var pullRequests = listOf<RelatedPullRequest>()

        override fun getRowCount(): Int = pullRequests.size

        override fun getColumnCount(): Int = 5

        override fun getColumnName(columnIndex: Int): String =
            when (columnIndex) {
                idColumn -> ID_COLUMN
                titleColumn -> TITLE_COLUMN
                authorColumn -> AUTHOR_COLUMN
                stateColumn -> STATE_COLUMN
                createdDateColumn -> CREATED_COLUMN
                else -> "".also { log.error("Unexpected column index '$columnIndex' in the pull requests table") }
            }

        override fun getValueAt(rowIndex: Int, columnIndex: Int): Any {
            val pullRequest = pullRequests[rowIndex]
            return when (columnIndex) {
                idColumn -> pullRequest.id
                titleColumn -> pullRequest.title
                authorColumn -> pullRequest.authorName ?: "Unknown"
                stateColumn -> pullRequest.state.toLowerCase().capitalize()
                createdDateColumn -> pullRequest.createdDate.displayValue()
                else -> "".also { log.error("Unexpected column index '$columnIndex' in the pull requests table") }
            }
        }

        private fun ZonedDateTime.displayValue() = withZoneSameInstant(ZoneId.systemDefault()).format(dateFormatter)
    }

    private fun TableColumn.fixedWidthCentered(width: Int) {
        cellRenderer = centeredCellRenderer
        resizable = false
        minWidth = width
        maxWidth = width
    }

    private fun prDetailsStyle() = """
        <style>
        body, div, td, p, a {margin: 10; font-family:'Dialog'; font-size:13pt;}
        pre {background: #f5f5f5;}
        code {background: #f5f5f5; border-radius: 3px; font-size: 85%; padding-top: 0.2em; padding-bottom: 0.2em;}
        </style>
        """.trimIndent()

    private abstract class SelectedPullRequestAction(
        text: String, description: String, icon: Icon,
        private val pullRequestProvider: () -> RelatedPullRequest?
    ) : DumbAwareAction(text, description, icon) {

        override fun update(event: AnActionEvent) {
            val pr = pullRequestProvider()
            event.presentation.isEnabled = pr != null
            event.presentation.isVisible = true
        }

        override fun actionPerformed(event: AnActionEvent) {
            val project = CommonDataKeys.PROJECT.getData(event.dataContext) ?: return
            val pullRequest = pullRequestProvider() ?: return

            actionPerformed(event, project, Link(pullRequest.link, "Pull Request #${pullRequest.id}"))
        }

        protected abstract fun actionPerformed(event: AnActionEvent, project: Project, link: Link)
    }

    private class CopySelectedPullRequestUrlAction(pullRequestProvider: () -> RelatedPullRequest?) :
        SelectedPullRequestAction(
            "Copy Bitbucket link", "Copy URL for the selected Pull Request",
            PullRequestCopyUrl, pullRequestProvider
        ) {
        init {
            copyShortcutFrom(ActionManager.getInstance().getAction("Bitbucket.CopyFileUrlInBitbucket"))
        }

        override fun actionPerformed(event: AnActionEvent, project: Project, link: Link) {
            copyToClipboard(project, link)
            service<UsageLog>().event("LinkToPullRequest", event.place, cloudEventAttributes)
        }
    }

    private class OpenSelectedPullRequestInBrowserAction(pullRequestProvider: () -> RelatedPullRequest?) :
        SelectedPullRequestAction(
            "Show in Bitbucket", "Open selected Pull Request in Bitbucket",
            PullRequestOpen, pullRequestProvider
        ) {
        init {
            copyShortcutFrom(ActionManager.getInstance().getAction("Bitbucket.OpenFileInBitbucket"))
        }

        override fun actionPerformed(event: AnActionEvent, project: Project, link: Link) {
            openUriInBrowser(project, link)
            service<UsageLog>().event("OpenPullRequest", event.place, cloudEventAttributes)
        }
    }
}

private fun BitbucketRepository.Cloud.commit(commit: Revision) = baseUri.resolve("commits/$commit")

private val commonmarkHtmlRenderer = HtmlRenderer.builder().build()
private val commonmarkParser = Parser.builder().build()

private fun RelatedPullRequest.html(): String = """
        <h4>$title</h4>
        <div class="branches">
            <code>$sourceBranchName</code> &rArr; <code>$destinationBranchName</code>
        </div>
        <hr/>
        ${description?.let { commonmarkHtmlRenderer.render(commonmarkParser.parse(it)) } ?: ""}
        """.trimIndent()
