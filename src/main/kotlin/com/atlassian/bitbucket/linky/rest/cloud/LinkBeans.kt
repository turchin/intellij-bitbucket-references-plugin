package com.atlassian.bitbucket.linky.rest.cloud

import java.net.URI

data class HtmlLinksBean(val html: LinkBean) {
    fun uri(): URI = URI.create(html.href)
}

data class LinkBean(val href: String)
