package com.atlassian.bitbucket.linky.repository

import com.atlassian.bitbucket.linky.UriScheme
import com.atlassian.bitbucket.linky.discovery.RemoteUrl
import com.atlassian.bitbucket.linky.logger

private val log = logger()

object GitRemoteUrlParser {
    fun parseRemoteUrl(url: String): RemoteUrl? {
        var urlToParse = url.toLowerCase()
        if (!urlToParse.contains("://")) {
            urlToParse = "ssh://$urlToParse"
        }
        try {
            return parseRemoteCoordinates(java.net.URI(urlToParse))
        } catch (e: java.net.URISyntaxException) {
            log.warn("Failed to parse URI '$url' -> '$urlToParse'")
            return null
        }
    }

    private fun parseRemoteCoordinates(uri: java.net.URI): RemoteUrl? {
        val scheme = UriScheme.forName(uri.scheme)

        if (scheme == null) {
            log.warn("Unknown scheme '${uri.scheme}' in the URI '$uri'")
            return null
        }

        var host = uri.host
        val port = uri.port
        val authority = uri.authority
        var path = uri.path?.removeSuffix(".git") ?: ""

        // host might be null for Bitbucket Cloud SSH URLs
        if (host == null && authority != null) {
            val colonIndex = authority.lastIndexOf(':')
            if (colonIndex > 0) {
                host = authority.substring(authority.indexOf('@') + 1, colonIndex)
                path = authority.substring(colonIndex + 1) + path
            }
        }

        path = path.removeSuffix("/")
        if (!path.startsWith("/")) {
            path = "/$path"
        }

        if (host == null) {
            log.debug("No hostname parsed from the URI '$uri'")
            return null
        }

        return RemoteUrl(scheme, host, port, path)
    }
}
