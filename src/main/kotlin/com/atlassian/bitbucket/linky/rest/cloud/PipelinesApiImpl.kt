package com.atlassian.bitbucket.linky.rest.cloud

import com.atlassian.bitbucket.linky.rest.mapException
import com.atlassian.bitbucket.linky.rest.pagination.Page
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.gson.responseObject
import com.google.gson.Gson
import java.util.*
import java.util.concurrent.CompletableFuture

private const val PAGE_SIZE = 10

class PipelinesApiImpl(
    private val fuel: FuelManager,
    private val gson: Gson,
    private val instance: BitbucketCloud,
    private val repositoryId: RepositoryId
) : PipelinesApi {

    override fun page(pageNumber: Int): CompletableFuture<CloudPage<Pipeline>> {
        val future = CompletableFuture<CloudPage<Pipeline>>()
        fuel.get(
            instance.pipelinesPageUrl(repositoryId).toString(),
            listOf(
                "pagelen" to PAGE_SIZE,
                "page" to pageNumber
            )
        ).responseObject<PagedBean<PipelineBean>>(gson) { _, _, result ->
            result.fold(
                success = { future.complete(it.toPipelinesPage(PAGE_SIZE)) },
                failure = { future.completeExceptionally(it.mapException("pipelines")) }
            )
        }
        return future
    }

    private fun PagedBean<PipelineBean>.toPipelinesPage(requestedPageSize: Int): CloudPage<Pipeline> {
        // Pipelines API returns incorrect `pagelen` and no `next` or `previous` links
        // so we do some heuristics here to calculate them

        // Size is returned by this API endpoint
        val size = this.size!!
        // There's next page only if this page is full, and we covered less items than total
        val hasNextPage = (requestedPageSize == values.size) && (page * requestedPageSize < size)

        fun pageFetcher(pageNumber: Int) = { api: BitbucketCloudApi ->
            api.repository(repositoryId).pipelines().page(pageNumber)
        }

        return Page(
            items = values.map { it.toPipeline() },
            pageSize = requestedPageSize,
            pageNumber = page,
            totalSize = size,
            nextPage = if (hasNextPage) pageFetcher(page + 1) else null
        )
    }
}


private fun BitbucketCloud.pipelinesPageUrl(repositoryId: RepositoryId) =
    apiBaseUrl.resolve("2.0/repositories/${repositoryId.urlPresentation}/pipelines/")

private data class PipelineBean(val uuid: String) {
    fun toPipeline() =
        Pipeline(UUID.fromString(uuid.trim('{', '}')))
}
