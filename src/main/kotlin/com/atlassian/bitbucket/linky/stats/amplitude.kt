package com.atlassian.bitbucket.linky.stats

import com.atlassian.bitbucket.linky.rest.configureFuelHttpClient
import com.github.kittinunf.fuel.core.HttpException
import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import com.google.gson.JsonPrimitive
import com.google.gson.JsonSerializer
import com.intellij.openapi.application.ApplicationManager
import java.net.URI
import java.time.Instant
import java.util.concurrent.CompletableFuture

data class AmplitudeEvent(
    val deviceId: String,
    val eventType: String,
    val time: Instant,
    val platform: String,
    val appVersion: String,
    val osName: String,
    val osVersion: String,
    val eventProperties: Map<String, String> = mapOf(),
    val userProperties: Map<String, String> = mapOf()
)

interface AmplitudeClient {
    @Throws(AmplitudeException::class)
    fun sendEvents(events: Collection<AmplitudeEvent>): CompletableFuture<Void>
}

class AmplitudeException(message: String? = null, cause: Throwable? = null) : RuntimeException(message, cause)

private val AMPLITUDE_API_URL = URI.create("https://api.amplitude.com/httpapi")

class DefaultAmplitudeClient : AmplitudeClient {

    private val apiKey =
        if (ApplicationManager.getApplication().isInternal) "35c5a9fae64c76956f586bc0f97dc0a8" else "f29e96dfe10d70c847f65062891f7533"
    private val fuel = configureFuelHttpClient(AMPLITUDE_API_URL)
    private val gson = createGson()

    override fun sendEvents(events: Collection<AmplitudeEvent>): CompletableFuture<Void> {
        if (events.isEmpty()) return CompletableFuture.completedFuture(null)

        val future = CompletableFuture<Void>()
        fuel.post(
            AMPLITUDE_API_URL.toString(), listOf(
                "api_key" to apiKey,
                "event" to events.joinToString(
                    separator = ",",
                    prefix = "[",
                    postfix = "]",
                    transform = { gson.toJson(it) })
            )
        ).response { _, _, result ->
            result.fold(
                success = { future.complete(null) },
                failure = {
                    future.completeExceptionally(
                        when (val error = it.exception) {
                            is HttpException ->
                                AmplitudeException("Got ${it.response.statusCode} error response from Amplitude: ${it.response.responseMessage}")
                            else ->
                                AmplitudeException("Failed to request Amplitude", error)
                        }
                    )
                }
            )
        }
        return future
    }
}

private fun createGson() = GsonBuilder()
    .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
    .registerTypeAdapter(Instant::class.java, JsonSerializer<Instant> { instant, _, _ ->
        JsonPrimitive(instant.toEpochMilli())
    })
    .create()
