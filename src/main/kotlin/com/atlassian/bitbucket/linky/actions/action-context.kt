package com.atlassian.bitbucket.linky.actions

import com.atlassian.bitbucket.linky.*
import com.atlassian.bitbucket.linky.blame.lineBlamer
import com.atlassian.bitbucket.linky.discovery.BitbucketRepositoriesService
import com.intellij.diff.requests.ContentDiffRequest
import com.intellij.diff.tools.util.DiffDataKeys
import com.intellij.dvcs.repo.Repository
import com.intellij.dvcs.repo.VcsRepositoryManager
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.CommonDataKeys
import com.intellij.openapi.actionSystem.DataContext
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import com.intellij.openapi.util.io.FileUtil
import com.intellij.openapi.vcs.*
import com.intellij.openapi.vcs.changes.Change
import com.intellij.openapi.vcs.changes.ContentRevision
import com.intellij.openapi.vcs.vfs.ContentRevisionVirtualFile
import com.intellij.openapi.vcs.vfs.VcsVirtualFile
import com.intellij.openapi.vfs.VirtualFile
import com.intellij.vcs.log.VcsLogDataKeys
import com.intellij.vcsUtil.VcsUtil


private val log = logger()

sealed class LinkyActionContext {
    abstract fun getBitbucketRepository(): BitbucketRepository?
    abstract fun getLinky(): BitbucketLinky?
}

data class LinkyFileActionContext(val linkyFile: LinkyFile) : LinkyActionContext() {
    val project = linkyFile.repository.project
    override fun getBitbucketRepository() =
        project.getComponent(BitbucketRepositoriesService::class.java).getBitbucketRepository(linkyFile.repository)

    override fun getLinky() =
        linkyFile.repository.project.service<BitbucketLinkyProvider>().getBitbucketLinky(linkyFile.repository)
}

data class LinkyCommitActionContext(
    val repository: Repository,
    val revision: Revision
) : LinkyActionContext() {
    val project = repository.project
    override fun getBitbucketRepository() =
        project.getComponent(BitbucketRepositoriesService::class.java).getBitbucketRepository(repository)

    override fun getLinky() = repository.project.service<BitbucketLinkyProvider>().getBitbucketLinky(repository)
}

data class LinkyRepositoryActionContext(val repository: Repository) : LinkyActionContext() {
    val project = repository.project
    override fun getBitbucketRepository() =
        project.getComponent(BitbucketRepositoriesService::class.java).getBitbucketRepository(repository)

    override fun getLinky() = project.service<BitbucketLinkyProvider>().getBitbucketLinky(repository)
}

object EmptyLinkyActionContext : LinkyActionContext() {
    override fun getBitbucketRepository() = null
    override fun getLinky() = null
}

interface LinkyActionContextProvider {
    fun getLinkyActionContext(event: AnActionEvent): LinkyActionContext
}

class CachingLinkyActionContextProvider(private val project: Project) : LinkyActionContextProvider {
    private val vcsManager = ProjectLevelVcsManager.getInstance(project)
    private val vcsRepositoryManager = VcsRepositoryManager.getInstance(project)
    private val bitbucketRepositoriesService = project.getComponent(BitbucketRepositoriesService::class.java)

    override fun getLinkyActionContext(event: AnActionEvent): LinkyActionContext {
        val context = event.dataContext
        val contextProject = CommonDataKeys.PROJECT.getData(context) ?: return EmptyLinkyActionContext
        if (contextProject != project) {
            log.warn("expected project '${project.name}, context project is '${contextProject.name}'")
            return EmptyLinkyActionContext
        }

        // Try to get specific Change list view context
        return gatherChangeListViewFileActionContext(context) ?:
        // Try to get specific Diff view context
        gatherDiffViewFileActionContext(context) ?:
        // Try to get commit context from VCS log tool window
        gatherVcsLogActionContext(context) ?:
        // Try to get normal file action context
        gatherFileActionContext(context) ?:
        // Finally, try to get global repository context
        gatherRepositoryActionContext() ?:
        // No suitable context for Linky
        EmptyLinkyActionContext
    }

    private fun gatherFileActionContext(context: DataContext): LinkyFileActionContext? {
        val file = CommonDataKeys.VIRTUAL_FILE.getData(context) ?: return null

        val filePath: FilePath?
        val repository: Repository?
        val fileRevision: String?
        if (file is VcsVirtualFile) {
            filePath = VcsUtil.getFilePath(file.path)
            repository = repositoryForFile(filePath)
            fileRevision = file.fileRevision?.revisionNumber?.revisionHash
        } else if (file is ContentRevisionVirtualFile) {
            filePath = file.contentRevision.file
            repository = repositoryForFile(filePath)
            fileRevision = file.contentRevision.revisionNumber.revisionHash
        } else {
            filePath = VcsUtil.getFilePath(file.path)
            repository = repositoryForFile(filePath)
            fileRevision = VcsDataKeys.VCS_FILE_REVISION.getData(context)?.revisionNumber?.revisionHash
                ?: repository?.let { it.currentRevision ?: it.currentBranchName }
        }

        if (filePath == null || repository == null || fileRevision == null || !fileTrackedStatus(project, file)) {
            return null
        }

        return createLinkyFileActionContext(repository, file, filePath, fileRevision)
    }

    private fun fileTrackedStatus(project: Project, file: VirtualFile) =
        FileStatusManager.getInstance(project)?.getStatus(file) !in listOf(FileStatus.UNKNOWN, FileStatus.IGNORED)

    private fun gatherChangeListViewFileActionContext(context: DataContext): LinkyFileActionContext? {
        val changes = VcsDataKeys.SELECTED_CHANGES.getData(context)
        val change = if (changes?.size == 1) changes[0] else return null

        val contentRevision = if (change.type === Change.Type.DELETED) change.beforeRevision else change.afterRevision

        return createLinkyFileActionContext(contentRevision)
    }

    private fun gatherDiffViewFileActionContext(context: DataContext): LinkyFileActionContext? {
        val diffRequest = DiffDataKeys.DIFF_REQUEST.getData(context) ?: return null
        val currentContent = DiffDataKeys.CURRENT_CONTENT.getData(context) ?: return null
        val currentChange = VcsDataKeys.CURRENT_CHANGE.getData(context) ?: return null

        if (diffRequest is ContentDiffRequest) {
            val indexOfCurrentContent = diffRequest.contents.indexOf(currentContent)
            val contentRevision =
                if (indexOfCurrentContent == 0) {
                    currentChange.beforeRevision
                } else if (indexOfCurrentContent == 1) {
                    currentChange.afterRevision
                } else return null

            return createLinkyFileActionContext(contentRevision)
        }

        return null
    }

    private fun createLinkyFileActionContext(contentRevision: ContentRevision?): LinkyFileActionContext? {
        val filePath = contentRevision?.file ?: return null
        val file = ContentRevisionVirtualFile.create(contentRevision)
        val repository = repositoryForFile(filePath) ?: return null
        val fileRevision = contentRevision.revisionNumber.revisionHash ?: return null

        return createLinkyFileActionContext(repository, file, filePath, fileRevision)

    }

    private fun createLinkyFileActionContext(
        repository: Repository,
        file: VirtualFile,
        filePath: FilePath,
        fileRevision: Revision
    ): LinkyFileActionContext? {
        val relativePath = FileUtil.getRelativePath(repository.root.path, filePath.path, '/') ?: return null
        return LinkyFileActionContext(
            LinkyFile(
                file,
                repository,
                fileRevision,
                relativePath,
                repository.lineBlamer()
            )
        )
    }

    private fun gatherVcsLogActionContext(context: DataContext): LinkyCommitActionContext? {
        // Try to get commit from the VCS log
        val vcsLog = VcsLogDataKeys.VCS_LOG.getData(context) ?: return null
        val selectedCommits = vcsLog.selectedCommits

        return if (selectedCommits.size == 1) {
            val commit = selectedCommits.first()
            val filePath = VcsUtil.getFilePath(commit.root.path)
            val repository = repositoryForFile(filePath) ?: return null
            LinkyCommitActionContext(repository, commit.hash.asString())
        } else null
    }

    private fun repositoryForFile(filePath: FilePath) =
        vcsRepositoryManager.getRepositoryForRoot(vcsManager.getVcsRootFor(filePath))

    private fun gatherRepositoryActionContext(): LinkyRepositoryActionContext? =
    // If there's only one recognized remote Bitbucket repository,
        // it is considered as a context for Linky actions
        vcsRepositoryManager.repositories
            .flatMap { repo -> bitbucketRepositoriesService.getBitbucketRepositories(repo).values }
            .distinct()
            .singleOrNull()
            ?.let { bbRepo -> LinkyRepositoryActionContext(bbRepo.repository) }
}

fun AnActionEvent.linkyActionContext() =
    project?.service<LinkyActionContextProvider>()?.getLinkyActionContext(this) ?: EmptyLinkyActionContext
