package com.atlassian.bitbucket.linky.rest.server

import com.atlassian.bitbucket.linky.rest.mapException
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.gson.responseObject
import com.google.gson.Gson
import java.net.URI
import java.util.concurrent.CompletableFuture

class ConnectivityTestApiImpl(
    private val fuel: FuelManager,
    private val gson: Gson,
    private val instance: BitbucketServer
) : ConnectivityTestApi {

    override fun authenticatedResource(): CompletableFuture<Void> {
        val future = CompletableFuture<Void>()
        fuel.get(instance.inboxPullRequestsCountUrl.toString())
            .responseObject<InboxPullRequestsCountBean>(gson) { _, _, result ->
                result.fold(
                    success = { future.complete(null) },
                    failure = { future.completeExceptionally(it.mapException("inbox pull request count")) }
                )
            }
        return future
    }

    override fun unauthenticatedResource(): CompletableFuture<Void> {
        val future = CompletableFuture<Void>()
        fuel.get(instance.applicationPropertiesUrl.toString())
            .responseObject<ApplicationPropertiesBean>(gson) { _, _, result ->
                result.fold(
                    success = { future.complete(null) },
                    failure = { future.completeExceptionally(it.mapException("application properties")) }
                )
            }
        return future
    }
}

private val BitbucketServer.applicationPropertiesUrl: URI
    get() = apiBaseUrl.resolve("rest/api/1.0/application-properties")

private val BitbucketServer.inboxPullRequestsCountUrl: URI
    get() = apiBaseUrl.resolve("rest/api/1.0/inbox/pull-requests/count")

private data class InboxPullRequestsCountBean(val count: Int)

private data class ApplicationPropertiesBean(
    val version: String,
    val buildNumber: String,
    val buildDate: String,
    val displayName: String
)
