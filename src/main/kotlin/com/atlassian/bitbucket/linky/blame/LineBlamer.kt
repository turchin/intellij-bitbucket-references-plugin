package com.atlassian.bitbucket.linky.blame

import com.atlassian.bitbucket.linky.LinkyFile
import com.atlassian.bitbucket.linky.RelativePath
import com.intellij.dvcs.repo.Repository
import com.intellij.openapi.extensions.ExtensionPointName

interface LineBlamerProvider {
    fun getLineBlamer(repository: Repository): LineBlamer?

    companion object {
        val EP_NAME =
            ExtensionPointName.create<LineBlamerProvider>("com.atlassian.bitbucket.linky.lineBlamer")
    }
}

interface LineBlamer {

    /**
     * Calculates a path to the file and a line number that together correspond to the requested line
     * of current version of the file.
     * Practically, does {@code blame} for a specific line and gets resulting file (might differ from
     * the requested if that file has been renamed later than the line was changed last time)
     * and number of the line that corresponds to the requested one.
     */
    fun blameLine(linkyFile: LinkyFile, lineNumber: Int): Pair<RelativePath, Int>? = null
}

fun Repository.lineBlamer(): LineBlamer =
    LineBlamerProvider.EP_NAME.extensions
        .mapNotNull { it.getLineBlamer(this) }
        .firstOrNull()
        ?: object : LineBlamer {}
