package com.atlassian.bitbucket.linky.rest.server

import com.atlassian.bitbucket.linky.rest.auth.PersonalAccessToken
import java.util.concurrent.CompletableFuture

interface PersonalAccessTokensApi {
    fun create(name: String, permissions: Iterable<Permission>): CompletableFuture<PersonalAccessToken>
}


enum class AccessLevel {
    READ, WRITE, ADMIN
}

sealed class Permission(internal open val entity: String, open val accessLevel: AccessLevel) {

    fun getCode() = "${entity}_${accessLevel.name}"

    data class ProjectPermission(override val accessLevel: AccessLevel) : Permission("PROJECT", accessLevel)
    data class RepositoryPermission(override val accessLevel: AccessLevel) : Permission("REPO", accessLevel)
    data class UnknownEntityPermission(
        override val entity: String,
        override val accessLevel: AccessLevel
    ) : Permission(entity, accessLevel)
}
