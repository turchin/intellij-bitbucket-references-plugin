package com.atlassian.bitbucket.linky.rest

import com.atlassian.bitbucket.linky.rest.auth.Authentication.*
import com.atlassian.bitbucket.linky.rest.auth.OAuthTokenManager
import com.atlassian.bitbucket.linky.rest.auth.OAuthTokens
import com.atlassian.bitbucket.linky.rest.cloud.BitbucketCloud
import com.atlassian.bitbucket.linky.rest.cloud.BitbucketCloudApi
import com.atlassian.bitbucket.linky.rest.cloud.BitbucketCloudApiImpl
import com.atlassian.bitbucket.linky.rest.cloud.config.OAuth3LOConfigurer
import com.atlassian.bitbucket.linky.rest.cloud.config.OAuthTokensHolder
import com.atlassian.bitbucket.linky.rest.cloud.config.oAuthConsumer
import com.atlassian.bitbucket.linky.rest.server.BitbucketServer
import com.atlassian.bitbucket.linky.rest.server.BitbucketServerApi
import com.atlassian.bitbucket.linky.rest.server.BitbucketServerApiImpl
import com.atlassian.bitbucket.linky.rest.server.config.PersonalAccessTokenConfigurer
import com.atlassian.bitbucket.linky.rest.server.config.PersonalAccessTokenHolder
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import java.util.concurrent.CompletableFuture

interface BitbucketRestClientProvider {
    fun bitbucketCloudRestClient(
        cloud: BitbucketCloud,
        authenticatedActionDescription: String? = null
    ): CompletableFuture<BitbucketCloudApi?>

    fun bitbucketServerRestClient(
        server: BitbucketServer,
        authenticatedActionDescription: String? = null
    ): CompletableFuture<BitbucketServerApi?>
}

class DefaultBitbucketRestClientProvider(
    private val project: Project
) : BitbucketRestClientProvider {

    override fun bitbucketCloudRestClient(
        cloud: BitbucketCloud,
        authenticatedActionDescription: String?
    ): CompletableFuture<BitbucketCloudApi?> {
        val maybeAuth = if (authenticatedActionDescription != null) {
            oAuthAuthentication(cloud, authenticatedActionDescription)
        } else {
            CompletableFuture.completedFuture(Guest)
        }

        return maybeAuth.thenApply {
            it?.let { auth ->
                BitbucketCloudApiImpl(
                    createFuel = { configureFuelHttpClient(cloud.baseUrl) }
                ) {
                    instance = cloud
                    authentication = auth
                }
            }
        }
    }

    override fun bitbucketServerRestClient(
        server: BitbucketServer,
        authenticatedActionDescription: String?
    ): CompletableFuture<BitbucketServerApi?> {
        val maybeAuth = if (authenticatedActionDescription != null) {
            accessTokenAuthentication(server, authenticatedActionDescription)
        } else {
            CompletableFuture.completedFuture(Guest)
        }

        return maybeAuth.thenApply {
            it?.let { auth ->
                BitbucketServerApiImpl(
                    createFuel = { configureFuelHttpClient(server.baseUrl) }
                ) {
                    instance = server
                    authentication = auth
                }
            }
        }
    }

    private fun oAuthAuthentication(cloud: BitbucketCloud, actionDescription: String): CompletableFuture<OAuth?> {
        val oAuthTokensHolder = service<OAuthTokensHolder>()

        val oAuthConfigured = when (oAuthTokensHolder.getOAuthTokens(cloud)) {
            null -> project.service<OAuth3LOConfigurer>()
                .configureOAuth3LO(cloud, actionDescription)
                .thenApply { null != it?.also { tokens -> oAuthTokensHolder.saveOAuthTokens(cloud, tokens) } }
            else -> CompletableFuture.completedFuture(true)
        }

        return oAuthConfigured.thenApply {
            when (it) {
                false -> null
                true -> OAuth(cloud.oAuthConsumer, object : OAuthTokenManager {
                    override fun getOAuthTokens(): OAuthTokens =
                        oAuthTokensHolder.getOAuthTokens(cloud)
                            ?: throw IllegalStateException("OAuth tokens not configured")

                    override fun saveOAuthTokens(oAuthTokens: OAuthTokens.RefreshAndAccessTokens) {
                        oAuthTokensHolder.saveOAuthTokens(cloud, oAuthTokens)
                    }
                })
            }
        }
    }

    private fun accessTokenAuthentication(
        server: BitbucketServer,
        actionDescription: String
    ): CompletableFuture<AccessToken?> {
        val personalAccessTokenHolder = service<PersonalAccessTokenHolder>()
        val accessToken = personalAccessTokenHolder.getPersonalAccessToken(server)
        val maybeToken = if (accessToken == null) {
            val personalAccessTokenConfigurer = project.service<PersonalAccessTokenConfigurer>()
            personalAccessTokenConfigurer.configurePersonalAccessToken(server, actionDescription)
        } else {
            CompletableFuture.completedFuture(accessToken)
        }
        return maybeToken.thenApply { token ->
            token?.let { AccessToken(it) }
        }
    }
}

open class BitbucketAuthenticationException(message: String) : RuntimeException(message)
