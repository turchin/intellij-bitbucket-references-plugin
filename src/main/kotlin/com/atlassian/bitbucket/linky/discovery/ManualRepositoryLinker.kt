package com.atlassian.bitbucket.linky.discovery

import com.atlassian.bitbucket.linky.hosting.BitbucketCloudRegistry
import com.atlassian.bitbucket.linky.hosting.BitbucketServerRegistry
import com.atlassian.bitbucket.linky.rest.cloud.BitbucketCloud
import com.atlassian.bitbucket.linky.rest.server.BitbucketServer
import com.intellij.openapi.components.service

interface ManualRepositoryLinker {
    fun register(remoteUrl: RemoteUrl, cloud: BitbucketCloud)
    fun register(remoteUrl: RemoteUrl, server: BitbucketServer)
}

class DefaultManualRepositoryLinker : ManualRepositoryLinker {
    override fun register(remoteUrl: RemoteUrl, cloud: BitbucketCloud) {
        if (remoteUrl.matchesCloudPattern()) {
            val cloudRegistry = service<BitbucketCloudRegistry>()
            cloudRegistry.add(remoteUrl.scheme, remoteUrl.hostname, remoteUrl.port, cloud)
        } else {
            throw IllegalArgumentException("Remote URL path '${remoteUrl.path}' doesn't match Bitbucket Cloud pattern")
        }
    }

    override fun register(remoteUrl: RemoteUrl, server: BitbucketServer) {
        if (remoteUrl.matchesServerPattern()) {
            val (appPath, _, _) = remoteUrl.serverPathProjectAndSlug()
            val serverRegistry = service<BitbucketServerRegistry>()
            serverRegistry.add(remoteUrl.scheme, remoteUrl.hostname, remoteUrl.port, appPath, server)
        } else {
            throw IllegalArgumentException("Remote URL path '${remoteUrl.path}' doesn't match Bitbucket Server pattern")
        }
    }
}
