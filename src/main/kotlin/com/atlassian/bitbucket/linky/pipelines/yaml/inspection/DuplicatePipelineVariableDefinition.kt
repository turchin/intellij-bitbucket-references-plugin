package com.atlassian.bitbucket.linky.pipelines.yaml.inspection

import com.atlassian.bitbucket.linky.LinkyBundle.message
import com.atlassian.bitbucket.linky.pipelines.yaml.pattern.inPipelinesFilePattern
import com.atlassian.bitbucket.linky.pipelines.yaml.pattern.yamlKeyValuePattern
import com.intellij.codeInspection.LocalInspectionTool
import com.intellij.codeInspection.LocalQuickFix
import com.intellij.codeInspection.ProblemDescriptor
import com.intellij.codeInspection.ProblemHighlightType.GENERIC_ERROR_OR_WARNING
import com.intellij.codeInspection.ProblemsHolder
import com.intellij.openapi.project.Project
import com.intellij.patterns.ElementPattern
import com.intellij.patterns.PlatformPatterns.psiElement
import com.intellij.psi.PsiElementVisitor
import com.intellij.util.containers.MultiMap
import org.jetbrains.yaml.psi.*


class DuplicatePipelineVariableDefinition : LocalInspectionTool() {

    override fun buildVisitor(
        holder: ProblemsHolder,
        isOnTheFly: Boolean
    ): PsiElementVisitor = object : YamlPsiElementVisitor() {
        override fun visitKeyValue(keyValue: YAMLKeyValue) {
            if (pipelineVariablesDefinitionPattern.accepts(keyValue)) {
                val sequence = keyValue.children.singleOrNull() as? YAMLSequence ?: return
                val visitor = DuplicateVariableNameVisitor()
                for (item in sequence.items) {
                    item.acceptChildren(visitor)
                }
                visitor.registerProblemsForDuplicates(holder)
            }
        }
    }

    private val pipelineVariablesDefinitionPattern: ElementPattern<YAMLKeyValue> =
        psiElement(YAMLKeyValue::class.java)
            .and(inPipelinesFilePattern)
            .and(yamlKeyValuePattern("variables"))
            .withSuperParent(3, psiElement(YAMLSequence::class.java))
            .withChild(psiElement(YAMLSequence::class.java))
}

private class DuplicateVariableNameVisitor : YamlPsiElementVisitor() {
    private val occurrences = MultiMap<String, YAMLKeyValue>()

    override fun visitMapping(mapping: YAMLMapping) {
        mapping.acceptChildren(this)
    }

    override fun visitKeyValue(keyValue: YAMLKeyValue) {
        if (yamlKeyValuePattern("name").accepts(keyValue)) {
            occurrences.putValue(keyValue.valueText, keyValue)
        }
    }

    fun registerProblemsForDuplicates(holder: ProblemsHolder) {
        for (entry: Map.Entry<String, Collection<YAMLKeyValue>> in occurrences.entrySet()) {
            if (entry.value.size > 1) {
                for (duplicatedKeyValue in entry.value) {
                    val variableName = duplicatedKeyValue.valueText
                    holder.registerProblem(
                        duplicatedKeyValue,
                        message("inspections.duplicate.variable.problem.text", variableName),
                        GENERIC_ERROR_OR_WARNING,
                        RemoveDuplicatedVariableQuickFix(variableName)
                    )
                }
            }
        }
    }
}

private class RemoveDuplicatedVariableQuickFix(private val variableName: String) : LocalQuickFix {
    override fun getName(): String = message("inspections.duplicate.variable.problem.fix.name", variableName)

    override fun getFamilyName(): String = message("inspections.duplicate.variable.problem.fix.family.name")

    override fun applyFix(project: Project, descriptor: ProblemDescriptor) {
        val keyValue = descriptor.psiElement as? YAMLKeyValue ?: return
        val sequenceItem = keyValue.parent.parent as? YAMLSequenceItem ?: return

        sequenceItem.deleteAndPositionCaret()
    }
}
