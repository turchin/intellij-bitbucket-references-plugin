package com.atlassian.bitbucket.linky.rest.server.config

import com.atlassian.bitbucket.linky.rest.ForbiddenException
import com.atlassian.bitbucket.linky.rest.UnauthorizedException
import com.atlassian.bitbucket.linky.rest.auth.Authentication
import com.atlassian.bitbucket.linky.rest.auth.Authentication.AccessToken
import com.atlassian.bitbucket.linky.rest.auth.Authentication.Basic
import com.atlassian.bitbucket.linky.rest.auth.PersonalAccessToken
import com.atlassian.bitbucket.linky.rest.configureFuelHttpClient
import com.atlassian.bitbucket.linky.rest.server.AccessLevel.READ
import com.atlassian.bitbucket.linky.rest.server.BitbucketServer
import com.atlassian.bitbucket.linky.rest.server.BitbucketServerApi
import com.atlassian.bitbucket.linky.rest.server.BitbucketServerApiImpl
import com.atlassian.bitbucket.linky.rest.server.Permission.ProjectPermission
import com.atlassian.bitbucket.linky.rest.server.Permission.RepositoryPermission
import com.intellij.openapi.Disposable
import com.intellij.openapi.application.ModalityState
import com.intellij.openapi.components.service
import com.intellij.openapi.progress.EmptyProgressIndicator
import com.intellij.openapi.progress.ProgressIndicator
import com.intellij.openapi.progress.ProgressManager
import com.intellij.openapi.progress.Task
import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.DialogWrapper
import com.intellij.openapi.ui.ValidationInfo
import com.intellij.openapi.util.Disposer
import com.intellij.ui.AnimatedIcon
import com.intellij.ui.DocumentAdapter
import com.intellij.ui.JBColor
import com.intellij.ui.components.JBTextField
import com.intellij.ui.components.fields.ExtendableTextComponent
import com.intellij.ui.components.fields.ExtendableTextField
import com.intellij.ui.components.labels.LinkLabel
import com.intellij.ui.components.panels.Wrapper
import com.intellij.util.ui.JBUI
import com.intellij.util.ui.UI.PanelFactory.grid
import com.intellij.util.ui.UI.PanelFactory.panel
import com.intellij.util.ui.UIUtil
import java.awt.Component
import java.util.concurrent.CompletableFuture
import java.util.concurrent.TimeUnit
import javax.swing.*
import javax.swing.event.DocumentEvent

private const val DEFAULT_TOKEN_NAME = "Bitbucket Linky IntelliJ plugin"

class BitbucketServerLoginDialog(
    private val project: Project,
    private val server: BitbucketServer,
    parent: Component? = null
) : DialogWrapper(project, parent, false, IdeModalityType.PROJECT) {

    private val serverTextField = ExtendableTextField().apply {
        isEditable = false
        isEnabled = false
        isFocusable = false
        text = server.baseUrl.toString()
    }
    private val centerPanel = Wrapper()
    private val southAdditionalPanel = Wrapper().apply { JBUI.Borders.emptyRight(UIUtil.DEFAULT_HGAP) }

    private var passwordUi = LoginPasswordCredentialsUI()
    private var tokenUi = TokenCredentialsUI()

    private lateinit var currentUi: CredentialsUI

    private var progressIndicator: ProgressIndicator? = null
    private val progressIcon = AnimatedIcon.Default()
    private val progressExtension = ExtendableTextComponent.Extension { progressIcon }

    private var tokenAcquisitionError: ValidationInfo? = null

    internal lateinit var token: String

    init {
        title = "Log in to Bitbucket Server"
        applyUi(passwordUi)
        init()
        Disposer.register(disposable, Disposable { progressIndicator?.cancel() })
    }

    override fun doOKAction() {
        setBusy(true)
        tokenAcquisitionError = null

        service<ProgressManager>().runProcessWithProgressAsynchronously(object :
            Task.Backgroundable(project, "Not Visible") {
            override fun run(indicator: ProgressIndicator) {
                currentUi.acquireToken(server, indicator)
                    .thenApply { token = it.value }
                    .get(10, TimeUnit.SECONDS)
            }

            override fun onSuccess() {
                close(OK_EXIT_CODE, true)
                setBusy(false)
            }

            override fun onThrowable(error: Throwable) {
                startTrackingValidation()
                tokenAcquisitionError = currentUi.handleAcquireError(error)
                setBusy(false)
            }
        }, progressIndicator!!)
    }

    private fun setBusy(busy: Boolean) {
        if (busy) {
            if (!serverTextField.extensions.contains(progressExtension)) serverTextField.addExtension(progressExtension)
            progressIndicator?.cancel()
            progressIndicator = EmptyProgressIndicator(ModalityState.stateForComponent(window))
            Disposer.register(disposable, Disposable { progressIndicator?.cancel() })
        } else {
            serverTextField.removeExtension(progressExtension)
            progressIndicator = null
        }
        serverTextField.isEnabled = !busy
        currentUi.setBusy(busy)
    }

    override fun createSouthAdditionalPanel(): Wrapper = southAdditionalPanel
    override fun createCenterPanel() = centerPanel
    override fun getPreferredFocusedComponent(): JComponent = currentUi.getPreferredFocus()

    private fun applyUi(ui: CredentialsUI) {
        currentUi = ui
        centerPanel.setContent(currentUi.getPanel())
        southAdditionalPanel.setContent(currentUi.getSouthPanel())
        setOKButtonText(currentUi.getActionText())
        currentUi.getPreferredFocus().requestFocus()
        tokenAcquisitionError = null
    }

    override fun doValidateAll(): List<ValidationInfo> =
        listOf(currentUi.getValidator(), { tokenAcquisitionError }).mapNotNull { it() }

    private fun JTextField.resetTokenAcquisitionErrorOnChange() =
        document.addDocumentListener(object : DocumentAdapter() {
            override fun textChanged(e: DocumentEvent) {
                tokenAcquisitionError = null
            }
        })


    private inner class LoginPasswordCredentialsUI :
        CredentialsUI {
        private val usernameField = JBTextField().apply { resetTokenAcquisitionErrorOnChange() }
        private val passwordField = JPasswordField().apply { resetTokenAcquisitionErrorOnChange() }
        private val tokenNameField = JBTextField()
        private val contextHelp = JLabel()

        private val switchUiLink = LinkLabel.create("Have access token handy?") { applyUi(tokenUi) }

        init {
            contextHelp.apply {
                text = "Password is not saved and used only to acquire Bitbucket Server Personal access token."
                foreground = JBColor.GRAY
            }

            tokenNameField.text = DEFAULT_TOKEN_NAME
        }

        override fun getPanel() = grid()
            .add(panel(serverTextField).withLabel("Server:"))
            .add(panel(usernameField).withLabel("Username:"))
            .add(panel(passwordField).withLabel("Password:"))
            .add(panel(tokenNameField).withLabel("Token name:"))
            .add(panel(contextHelp)).createPanel()

        override fun getPreferredFocus() = usernameField

        override fun getValidator() = chain(
            {
                notBlank(
                    usernameField,
                    "Username cannot be empty"
                )
            },
            {
                notBlank(
                    passwordField,
                    "Password cannot be empty"
                )
            }
        )

        override fun getSouthPanel() = switchUiLink

        override fun getActionText() = "Acquire token"

        override fun acquireToken(
            server: BitbucketServer,
            indicator: ProgressIndicator
        ): CompletableFuture<PersonalAccessToken> {
            val username = usernameField.text
            val client = createClient(server, Basic(username, String(passwordField.password)))

            val userTokenName = tokenNameField.text
            val tokenName = if (userTokenName.isNullOrBlank()) tokenNameField.emptyText.text else userTokenName

            return client.personalAccessTokens(username)
                .create(tokenName, listOf(ProjectPermission(READ), RepositoryPermission(READ)))
        }

        override fun handleAcquireError(error: Throwable): ValidationInfo? = when (error) {
            is ForbiddenException -> ValidationInfo("Insufficient access level", usernameField)
            is UnauthorizedException -> ValidationInfo("Incorrect credentials", passwordField).withOKEnabled()
            else -> ValidationInfo(error.message ?: "Failed to validate personal access token").withOKEnabled()
        }

        override fun setBusy(busy: Boolean) {
            usernameField.isEnabled = !busy
            passwordField.isEnabled = !busy
            tokenNameField.isEnabled = !busy
            contextHelp.isEnabled = !busy
            switchUiLink.isEnabled = !busy
        }
    }

    private inner class TokenCredentialsUI :
        CredentialsUI {
        private val tokenField = JBTextField().apply { resetTokenAcquisitionErrorOnChange() }

        private val switchUiLink = LinkLabel.create("Log in with username and password") { applyUi(passwordUi) }

        override fun getPanel() = grid()
            .add(panel(serverTextField).withLabel("Server:"))
            .add(panel(tokenField).withLabel("Token:")).createPanel()

        override fun getPreferredFocus() = tokenField

        override fun getValidator(): Validator = {
            notBlank(
                tokenField,
                "Token cannot be empty"
            )
        }

        override fun getSouthPanel() = switchUiLink

        override fun getActionText() = "Check token"

        override fun acquireToken(
            server: BitbucketServer,
            indicator: ProgressIndicator
        ): CompletableFuture<PersonalAccessToken> {
            val token = PersonalAccessToken(tokenField.text)
            val client = createClient(server, AccessToken(token))

            return client.testConnectivity().authenticatedResource()
                .thenApply { token }
        }

        override fun handleAcquireError(error: Throwable): ValidationInfo? =
            when (error) {
                is ForbiddenException -> ValidationInfo("Insufficient access level", tokenField)
                is UnauthorizedException -> ValidationInfo(
                    "Incorrect personal access token",
                    tokenField
                ).withOKEnabled()
                else -> ValidationInfo(error.message ?: "Failed to validate personal access token").withOKEnabled()
            }

        override fun setBusy(busy: Boolean) {
            tokenField.isEnabled = !busy
            switchUiLink.isEnabled = !busy
        }
    }
}

private fun createClient(server: BitbucketServer, auth: Authentication): BitbucketServerApi {
    return BitbucketServerApiImpl(
        createFuel = { configureFuelHttpClient(server.baseUrl) }
    ) {
        instance = server
        authentication = auth
    }
}

typealias Validator = () -> ValidationInfo?

private fun chain(vararg validators: Validator): Validator =
    { validators.asSequence().mapNotNull { it() }.firstOrNull() }

private fun notBlank(textField: JTextField, message: String): ValidationInfo? =
    if (textField.text.isNullOrBlank()) ValidationInfo(message, textField) else null

private interface CredentialsUI {
    fun getPanel(): JPanel
    fun getSouthPanel(): JComponent
    fun getPreferredFocus(): JComponent
    fun getValidator(): Validator
    fun getActionText(): String

    fun acquireToken(server: BitbucketServer, indicator: ProgressIndicator): CompletableFuture<PersonalAccessToken>
    fun handleAcquireError(error: Throwable): ValidationInfo?

    fun setBusy(busy: Boolean)
}
