package com.atlassian.bitbucket.linky.utils

import com.atlassian.bitbucket.linky.UriScheme
import com.atlassian.bitbucket.linky.logger
import org.jdom.Element
import java.net.URI
import java.time.Instant
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException
import java.util.*

private val log = logger()

fun Element.getString(attributeName: String): String? = this.getAttributeValue(attributeName)

fun Element.getInt(attributeName: String) = this.getAttributeValue(attributeName)?.let {
    try {
        if (it.isNotBlank()) it.toInt() else 0
    } catch (e: NumberFormatException) {
        log.warn("Malformed integer '$it' in the saved state")
        return null
    }
}

fun Element.getUrl(attributeName: String) = this.getAttributeValue(attributeName)?.let {
    try {
        URI.create(it.appendTrailSlash())
    } catch (e: IllegalArgumentException) {
        log.warn("Malformed URL '$it' in the saved state")
        return null
    }
}

fun Element.getUriScheme(attributeName: String) = this.getAttributeValue(attributeName)?.let {
    try {
        UriScheme.valueOf(it.toUpperCase())
    } catch (e: IllegalArgumentException) {
        log.warn("Unknown URI scheme '$it' in the saved state")
        return null
    }
}

fun Element.getUuid(attributeName: String) = this.getAttributeValue(attributeName)?.let {
    try {
        UUID.fromString(it)
    } catch (e: IllegalArgumentException) {
        log.warn("Malformed UUID '$it' in the saved state")
        return null
    }
}

fun Element.getInstant(attributeName: String) =
    this.getAttributeValue(attributeName)?.let {
        try {
            Instant.from(DateTimeFormatter.ISO_INSTANT.parse(it))
        } catch (e: DateTimeParseException) {
            log.warn("Malformed timestamp '$it' in the saved state")
            return null
        }
    }
