package com.atlassian.bitbucket.linky.clipboard

import java.awt.datatransfer.DataFlavor
import java.awt.datatransfer.DataFlavor.*
import java.awt.datatransfer.Transferable
import java.awt.datatransfer.UnsupportedFlavorException
import java.net.URI

data class TransferableLink(val uri: URI, val text: String) : Transferable {
    private val dataFlavors = arrayOf(
        allHtmlFlavor,
        selectionHtmlFlavor,
        fragmentHtmlFlavor,
        stringFlavor
    )

    override fun getTransferData(flavor: DataFlavor?): String {
        val encodedUri = uri.toASCIIString()
        return when (flavor) {
            allHtmlFlavor, selectionHtmlFlavor, fragmentHtmlFlavor -> "<a href=\"$encodedUri\">$text</a>"
            stringFlavor -> encodedUri
            else -> throw UnsupportedFlavorException(flavor)
        }
    }

    override fun isDataFlavorSupported(flavor: DataFlavor?): Boolean =
        dataFlavors.contains(flavor)

    override fun getTransferDataFlavors(): Array<DataFlavor> =
        dataFlavors.copyOf()
}
