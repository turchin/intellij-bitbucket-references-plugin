package com.atlassian.bitbucket.linky.rest

import com.atlassian.bitbucket.linky.logger
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.core.HttpException
import java.util.concurrent.CompletableFuture
import java.util.concurrent.CompletionException
import javax.net.ssl.SSLException

open class BitbucketRestException(message: String, cause: Throwable? = null) : RuntimeException(message, cause)

open class BadResponseCodeException(val statusCode: Int, val statusText: String, val endpointName: String) :
    BitbucketRestException("Got response with $statusCode ($statusText) for $endpointName endpoint")

class UnauthorizedException(
    endpointName: String,
    statusText: String = "Unauthorized"
) : BadResponseCodeException(401, statusText, endpointName)


class ForbiddenException(
    endpointName: String,
    statusText: String = "Forbidden"
) : BadResponseCodeException(403, statusText, endpointName)


class NotFoundException(
    endpointName: String,
    statusText: String = "Not Found"
) : BadResponseCodeException(404, statusText, endpointName)


class MalformedResponseException : BitbucketRestException("Failed to parse response JSON")


fun Throwable.unwrap() = when (this) {
    is CompletionException -> cause ?: this
    else -> this
}


fun FuelError.mapException(
    endpointName: String,
    customStatusCodeError: (Int) -> BitbucketRestException? = { null }
) =
    when (val error = exception.unwrap()) {
        is HttpException ->
            with(response) {
                customStatusCodeError(statusCode) ?: when (statusCode) {
                    401 -> UnauthorizedException(endpointName, responseMessage)
                    403 -> ForbiddenException(endpointName, responseMessage)
                    404 -> NotFoundException(endpointName, responseMessage)
                    else -> BadResponseCodeException(statusCode, responseMessage, endpointName)
                }
            }
        is SSLException -> BitbucketRestException("SSL error", error)
        is IllegalStateException -> MalformedResponseException()
        else -> BitbucketRestException("Unrecognised Bitbucket client error", error)
    }


private val log = logger()

fun <T> CompletableFuture<T>.onBitbucketError(block: (BitbucketRestException) -> Unit): CompletableFuture<T> =
    whenComplete lambda@{ _, maybeError: Throwable? ->
        val error = maybeError?.unwrap() ?: return@lambda
        when (error) {
            is BitbucketRestException -> block(error)
            else -> log.error("Unrecognised exception from Bitbucket REST client", error)
        }
    }
