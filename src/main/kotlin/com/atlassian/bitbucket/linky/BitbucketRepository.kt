package com.atlassian.bitbucket.linky

import com.atlassian.bitbucket.linky.discovery.RemoteUrl
import com.atlassian.bitbucket.linky.rest.cloud.BitbucketCloud
import com.atlassian.bitbucket.linky.rest.server.BitbucketServer
import com.intellij.dvcs.repo.Repository
import java.net.URI

sealed class BitbucketRepository(
    val repository: Repository,
    val remoteUrl: RemoteUrl,
    val slug: String
) {
    abstract val baseUri: URI

    abstract val fullSlug: String

    class Cloud(
        repository: Repository,
        remoteUrl: RemoteUrl,
        val hosting: BitbucketCloud,
        val workspaceId: String,
        slug: String
    ) : BitbucketRepository(repository, remoteUrl, slug) {
        override val baseUri: URI
            get() = hosting.baseUrl.resolve("$fullSlug/")
        override val fullSlug: String
            get() = "$workspaceId/$slug"

        override fun toString(): String {
            return "BitbucketCloudRepository{$fullSlug}"
        }
    }

    class Server(
        repository: Repository,
        remoteUrl: RemoteUrl,
        val hosting: BitbucketServer,
        val projectKey: String,
        slug: String
    ) : BitbucketRepository(repository, remoteUrl, slug) {
        override val baseUri: URI
            get() = hosting.baseUrl.resolve("projects/$projectKey/repos/$slug/")
        override val fullSlug: String
            get() = "$projectKey/$slug"

        override fun toString(): String {
            return "BitbucketServerRepository{$fullSlug}"
        }
    }
}
