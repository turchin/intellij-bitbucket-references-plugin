import org.jetbrains.intellij.tasks.PatchPluginXmlTask
import org.jetbrains.intellij.tasks.PublishTask
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile


plugins {
    id("org.jetbrains.intellij") version "0.4.13"
    kotlin("jvm") version "1.3.61"
}

group = "com.atlassian.bitbucket.pipelines"
version = "8.1"

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))

    val commonmarkVersion = "0.13.0"
    val fuelVersion = "2.2.1"
    val nanoHttpdVersion = "2.3.1"
    val urlBuilderVersion = "2.0.9"

    implementation("com.atlassian.commonmark", "commonmark", commonmarkVersion)
    implementation("org.nanohttpd", "nanohttpd", nanoHttpdVersion)
    implementation("com.github.kittinunf.fuel", "fuel", fuelVersion)
    implementation("com.github.kittinunf.fuel", "fuel-gson", fuelVersion)
    implementation("io.mikael", "urlbuilder", urlBuilderVersion)

    val assertkVersion = "0.19"
    val jsonassertVersion = "1.5.0"
    val junitVersion = "5.5.1"
    val mockkVersion = "1.9.3"

    testImplementation("io.mockk", "mockk", mockkVersion)
    testImplementation("com.willowtreeapps.assertk", "assertk-jvm", assertkVersion)
    testImplementation("org.skyscreamer", "jsonassert", jsonassertVersion) {
        exclude("junit", "junit")
    }
    testImplementation("org.junit.jupiter", "junit-jupiter-api", junitVersion)
    testRuntimeOnly("org.junit.jupiter", "junit-jupiter-engine", junitVersion)
}

val releaseChannel: String = System.getenv("BITBUCKET_DEPLOYMENT_ENVIRONMENT") ?: "local"
val prodRelease = "production".equals(releaseChannel, ignoreCase = true)

intellij {
    version = "2019.2"
    setPlugins("yaml", "sh", "git4idea", "hg4idea")
}

tasks {
    withType<KotlinCompile> {
        kotlinOptions.apply {
            jvmTarget = "1.8"
            freeCompilerArgs = listOf("-Xinline-classes", "-Xuse-experimental=kotlin.Experimental")
        }
    }

    withType<PatchPluginXmlTask> {
        val buildNumber = System.getenv("BITBUCKET_BUILD_NUMBER") ?: "local"
        val buildType = when(releaseChannel) {
            "production" -> ""
            "eap" -> ".eap"
            else -> ".${releaseChannel.substring(0, 1)}"
        }
        version("${project.version}.$buildNumber$buildType")

        if (prodRelease) {
            untilBuild("193.*")
        } else {
            // Any non-production release is open-ended
            untilBuild(null)
        }

        changeNotes(
            """
            <h5>Fixed</h5>
            <ul>
              <li>Repository properties in heterogeneous multi-module projects</li>
            </ul>
            """.trimIndent()
        )
    }

    withType<PublishTask> {
        token(System.getenv("JB_API_TOKEN"))
        if (prodRelease.not()) {
            channels(releaseChannel)
        }
    }
}

repositories {
    jcenter()
}

tasks.test {
    useJUnitPlatform()
    testLogging {
        events("passed", "skipped", "failed")
    }
}
