package com.atlassian.bitbucket.linky.pipelines.yaml.completion

import com.atlassian.bitbucket.linky.pipelines.yaml.pattern.serviceDefinitionsPattern
import com.atlassian.bitbucket.linky.pipelines.yaml.reference.ServiceDefinitionReference
import com.intellij.codeInsight.completion.*
import com.intellij.codeInsight.lookup.LookupElementBuilder
import com.intellij.icons.AllIcons
import com.intellij.patterns.PlatformPatterns.psiElement
import com.intellij.util.ProcessingContext
import org.jetbrains.yaml.psi.YAMLMapping

class DefinitionCompletionContributor : CompletionContributor() {
    init {
        // Only provide completion for services, overriding predefined caches is probably not common
        extend(CompletionType.BASIC, inServiceDefinitionsPattern, ServiceDefinitionCompletionProvider)
    }
}

private val inServiceDefinitionsPattern =
    psiElement()
        .withSuperParent(2, YAMLMapping::class.java)
        .withSuperParent(3, serviceDefinitionsPattern)

private object ServiceDefinitionCompletionProvider : CompletionProvider<CompletionParameters>() {
    override fun addCompletions(
        parameters: CompletionParameters,
        context: ProcessingContext,
        result: CompletionResultSet
    ) {
        result.addAllElements(
            ServiceDefinitionReference.predefinedServices.map {
                LookupElementBuilder.create(it)
                    .withTypeText("Predefined value", AllIcons.Nodes.Field, false)
                    .withItemTextItalic(true)
            }
        )
    }
}
