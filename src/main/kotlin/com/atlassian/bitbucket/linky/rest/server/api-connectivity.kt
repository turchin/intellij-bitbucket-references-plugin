package com.atlassian.bitbucket.linky.rest.server

import java.util.concurrent.CompletableFuture

interface ConnectivityTestApi {
    fun authenticatedResource(): CompletableFuture<Void>
    fun unauthenticatedResource(): CompletableFuture<Void>
}
