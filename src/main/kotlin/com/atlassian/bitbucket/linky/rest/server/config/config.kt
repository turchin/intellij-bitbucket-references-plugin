package com.atlassian.bitbucket.linky.rest.server.config

import com.google.gson.*
import java.lang.reflect.Type
import java.time.Instant
import java.time.ZoneOffset
import java.time.ZonedDateTime

fun defaultGson(): Gson = GsonBuilder()
    .registerTypeAdapter(ZonedDateTime::class.java, object : JsonDeserializer<ZonedDateTime> {
        override fun deserialize(json: JsonElement, type: Type, ctx: JsonDeserializationContext): ZonedDateTime {
            return ZonedDateTime.ofInstant(Instant.ofEpochMilli(json.asJsonPrimitive.asLong), ZoneOffset.UTC)
        }
    })
    .create()
