package com.atlassian.bitbucket.linky.rest.auth

inline class PersonalAccessToken(val value: String)
