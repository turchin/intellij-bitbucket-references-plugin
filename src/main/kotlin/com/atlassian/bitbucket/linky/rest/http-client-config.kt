package com.atlassian.bitbucket.linky.rest

import com.atlassian.bitbucket.linky.logger
import com.atlassian.bitbucket.linky.stats.linkyVersion
import com.github.kittinunf.fuel.core.*
import com.intellij.util.net.HttpConfigurable
import com.intellij.util.net.ssl.CertificateManager
import java.net.InetSocketAddress
import java.net.Proxy
import java.net.URI

private val log = logger()

fun configureFuelHttpClient(baseUrl: URI): FuelManager =
    FuelManager().apply {
        addRequestInterceptor(LogRequestInterceptor)
        addResponseInterceptor(LogResponseInterceptor)

        val sslContext = CertificateManager.getInstance().sslContext
        socketFactory = sslContext.socketFactory
        proxy = with(HttpConfigurable.getInstance()) {
            if (USE_HTTP_PROXY && isHttpProxyEnabledForUrl(baseUrl.toString())) {
                val address = when (PROXY_HOST) {
                    null -> InetSocketAddress(PROXY_PORT)
                    else -> InetSocketAddress(PROXY_HOST, PROXY_PORT)
                }
                Proxy(Proxy.Type.HTTP, address)
            } else null
        }

        baseHeaders = mapOf(
            Headers.USER_AGENT to "BitbucketLinky/$linkyVersion"
        )
    }

object LogRequestInterceptor : FoldableRequestInterceptor {
    override fun invoke(next: RequestTransformer): RequestTransformer {
        return { request ->
            log.debug(request.toString())
            next(request)
        }
    }
}

object LogResponseInterceptor : FoldableResponseInterceptor {
    override fun invoke(next: ResponseTransformer): ResponseTransformer {
        return { request, response ->
            log.debug(response.toString())
            next(request, response)
        }
    }
}
