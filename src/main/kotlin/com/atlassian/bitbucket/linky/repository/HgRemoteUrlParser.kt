package com.atlassian.bitbucket.linky.repository

import com.atlassian.bitbucket.linky.UriScheme
import com.atlassian.bitbucket.linky.discovery.RemoteUrl
import com.atlassian.bitbucket.linky.logger
import java.net.URI
import java.net.URISyntaxException

private val log = logger()

object HgRemoteUrlParser {
    fun parseRemoteUrl(url: String): RemoteUrl? {
        val urlToParse = url.toLowerCase()
        try {
            return parseRemoteCoordinates(URI(urlToParse))
        } catch (e: URISyntaxException) {
            log.warn("Failed to parse URI '$url' -> '$urlToParse'")
            return null
        }
    }

    private fun parseRemoteCoordinates(uri: URI): RemoteUrl? {
        val scheme = UriScheme.forName(uri.scheme)

        if (scheme == null) {
            log.warn("Unknown scheme '${uri.scheme}' in the URI '$uri'")
            return null
        }

        val host = uri.host
        val port = uri.port
        var path = uri.path ?: ""

        if (host == null) {
            log.debug("No hostname parsed from the URI '$uri'")
            return null
        }

        path = path.removeSuffix("/")
        if (!path.startsWith("/")) {
            path = "/$path"
        }

        return RemoteUrl(scheme, host, port, path)
    }
}
