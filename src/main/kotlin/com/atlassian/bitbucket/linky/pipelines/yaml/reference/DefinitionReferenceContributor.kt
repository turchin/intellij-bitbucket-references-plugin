package com.atlassian.bitbucket.linky.pipelines.yaml.reference

import com.atlassian.bitbucket.linky.pipelines.yaml.pattern.inPipelinesFilePattern
import com.atlassian.bitbucket.linky.pipelines.yaml.pattern.yamlKeyValuePattern
import com.intellij.patterns.ElementPattern
import com.intellij.patterns.PlatformPatterns
import com.intellij.psi.*
import com.intellij.util.ProcessingContext
import org.jetbrains.yaml.psi.YAMLScalar
import org.jetbrains.yaml.psi.YAMLSequenceItem

class DefinitionReferenceContributor : PsiReferenceContributor() {

    override fun registerReferenceProviders(registrar: PsiReferenceRegistrar) {
        registrar.registerReferenceProvider(
            definitionUsageInStepPattern("caches"),
            DefinitionReferenceProvider { CacheDefinitionReference(it) })
        registrar.registerReferenceProvider(
            definitionUsageInStepPattern("services"),
            DefinitionReferenceProvider { ServiceDefinitionReference(it) })
    }

    private fun definitionUsageInStepPattern(type: String): ElementPattern<YAMLScalar> =
        PlatformPatterns.psiElement(YAMLScalar::class.java)
            .and(inPipelinesFilePattern)
            .withParent(YAMLSequenceItem::class.java)
            .withSuperParent(3, yamlKeyValuePattern(type))
            .withSuperParent(5, yamlKeyValuePattern("step"))
}

private class DefinitionReferenceProvider(
    private val createReference: (YAMLScalar) -> PsiReference
) : PsiReferenceProvider() {
    override fun getReferencesByElement(
        element: PsiElement,
        context: ProcessingContext
    ): Array<PsiReference> {
        val scalar = element as? YAMLScalar ?: return emptyArray()
        return arrayOf(createReference(scalar))
    }
}
