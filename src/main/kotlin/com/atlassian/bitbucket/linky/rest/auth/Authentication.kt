package com.atlassian.bitbucket.linky.rest.auth

import com.atlassian.bitbucket.linky.rest.auth.OAuthTokens.RefreshAndAccessTokens


sealed class Authentication {
    object Guest : Authentication()
    data class Basic(val username: String, val password: String) : Authentication()
    data class AccessToken(val token: PersonalAccessToken) : Authentication()
    data class OAuth(
        val consumer: OAuthConsumer,
        val tokenManager: OAuthTokenManager
    ) : Authentication()
}

interface OAuthTokenManager {
    fun getOAuthTokens(): OAuthTokens
    fun saveOAuthTokens(oAuthTokens: RefreshAndAccessTokens)
}
