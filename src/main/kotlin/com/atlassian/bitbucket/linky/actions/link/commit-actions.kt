package com.atlassian.bitbucket.linky.actions.link

import com.atlassian.bitbucket.linky.BitbucketRepository
import com.atlassian.bitbucket.linky.Revision
import com.atlassian.bitbucket.linky.actions.*
import com.atlassian.bitbucket.linky.gutter.DefaultGutterCommitDataProvider
import com.atlassian.bitbucket.linky.gutter.GutterCommitData
import com.atlassian.bitbucket.linky.gutter.GutterCommitDataProvider
import com.atlassian.bitbucket.linky.icons.BitbucketLinkyIcons.Actions.CommitCopyUrl
import com.atlassian.bitbucket.linky.icons.BitbucketLinkyIcons.Actions.CommitOpen
import com.atlassian.bitbucket.linky.repository.hasRevisionBeenPushed
import com.atlassian.bitbucket.linky.short
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.progress.ProgressIndicator
import com.intellij.openapi.progress.Task
import com.intellij.openapi.project.DumbAwareAction
import com.intellij.openapi.project.Project
import com.intellij.openapi.vcs.annotate.AnnotationGutterActionProvider
import com.intellij.openapi.vcs.annotate.FileAnnotation
import java.net.URI
import javax.swing.Icon

abstract class CommitLinkAction(text: String, description: String, icon: Icon) :
    DumbAwareAction(text, description, icon) {

    override fun actionPerformed(event: AnActionEvent) {
        val project = event.project ?: return
        val ctx = event.linkyActionContext()
        val bbRepo = ctx.getBitbucketRepository() ?: return
        val repository = bbRepo.repository

        object : Task.Backgroundable(project, "Calculating commit link...") {
            override fun run(indicator: ProgressIndicator) {
                getCommitViewUrl(event)?.let { (revision, uri) ->
                    val revisionNotPushed = repository.hasRevisionBeenPushed(revision).not()
                    if (revisionNotPushed) {
                        showLinkToLocalRevisionError(repository.project, repository)
                    }
                    val link = Link(uri, revision.short())
                    actionPerformed(repository.project, link, revisionNotPushed, event.place, bbRepo)
                }
            }
        }.queue()
    }

    override fun update(event: AnActionEvent) {
        if (event.project == null) {
            event.presentation.isEnabledAndVisible = false
        } else {
            val ctx = event.linkyActionContext()
            event.presentation.isVisible =
                (ctx is LinkyCommitActionContext || ctx is LinkyFileActionContext) && ctx.getLinky() != null
            event.presentation.isEnabled = event.presentation.isVisible && canPerformActionIfVisible(event)
        }
    }

    protected abstract fun actionPerformed(
        project: Project,
        link: Link,
        warn: Boolean,
        place: String,
        repository: BitbucketRepository
    )

    internal open fun canPerformActionIfVisible(event: AnActionEvent) = true

    internal open fun getCommitViewUrl(event: AnActionEvent): Pair<Revision, URI>? {
        val ctx = event.linkyActionContext()
        val revision =
            when (ctx) {
                is LinkyCommitActionContext -> ctx.revision
                is LinkyFileActionContext -> ctx.linkyFile.revision
                else -> return null
            }
        return ctx.getLinky()?.getCommitViewUri(revision)?.let { revision to it }
    }
}

open class CopyCommitLinkAction : CommitLinkAction(
    "Copy Bitbucket Commit link", "Copy URL for Bitbucket Commit view of the selected commit", CommitCopyUrl
) {
    override fun actionPerformed(
        project: Project,
        link: Link,
        warn: Boolean,
        place: String,
        repository: BitbucketRepository
    ) {
        copyToClipboard(project, link, warn)
        logActionUsage("CopyLinkToCommit", place, repository)
    }
}

open class OpenCommitInBitbucketAction : CommitLinkAction(
    "Open Commit in Bitbucket", "Open selected Commit in Bitbucket", CommitOpen
) {
    override fun actionPerformed(
        project: Project,
        link: Link,
        warn: Boolean,
        place: String,
        repository: BitbucketRepository
    ) {
        openUriInBrowser(project, link, warn)
        logActionUsage("OpenCommit", place, repository)
    }
}

class OpenCommitInBitbucketGutterActionProvider : AnnotationGutterActionProvider {
    override fun createAction(annotation: FileAnnotation): AnAction =
        object : OpenCommitInBitbucketAction(),
            GutterCommitDataProvider by DefaultGutterCommitDataProvider(annotation) {

            override fun canPerformActionIfVisible(event: AnActionEvent): Boolean =
                canPerformActionIfVisible(getGutterCommitData(event))

            override fun getCommitViewUrl(event: AnActionEvent) = getGutterCommitViewUrl(getGutterCommitData(event))
        }
}

class CopyCommitLinkGutterActionProvider : AnnotationGutterActionProvider {
    override fun createAction(annotation: FileAnnotation): AnAction =
        object : CopyCommitLinkAction(),
            GutterCommitDataProvider by DefaultGutterCommitDataProvider(annotation) {

            override fun canPerformActionIfVisible(event: AnActionEvent): Boolean =
                canPerformActionIfVisible(getGutterCommitData(event))

            override fun getCommitViewUrl(event: AnActionEvent) = getGutterCommitViewUrl(getGutterCommitData(event))
        }
}

private fun getGutterCommitViewUrl(gutterCommitData: GutterCommitData?): Pair<Revision, URI>? {
    val (ctx, actualLineNumber, lineRevision) = gutterCommitData ?: return null
    val linky = ctx.getLinky() ?: return null
    if (actualLineNumber == null || lineRevision == null) {
        return null
    }

    return linky.getCommitViewUri(lineRevision, ctx.linkyFile, actualLineNumber + 1).let { lineRevision to it }
}

private fun canPerformActionIfVisible(gutterCommitData: GutterCommitData?): Boolean =
    gutterCommitData?.let { it.actualLineNumber != null && it.revision != null } ?: false
