package com.atlassian.bitbucket.linky.rest.cloud

import com.atlassian.bitbucket.linky.rest.auth.Authentication
import com.atlassian.bitbucket.linky.rest.auth.Authentication.Guest
import com.atlassian.bitbucket.linky.rest.cloud.BitbucketCloud.Production
import com.atlassian.bitbucket.linky.rest.pagination.Page
import java.net.URI
import java.util.*
import java.util.concurrent.CompletableFuture

interface BitbucketCloudApi {
    fun repository(repositoryId: RepositoryId): RepositoryApi
    fun snippets(workspaceId: String? = null): SnippetsApi
}

typealias CloudPage<T> = Page<BitbucketCloudApi, T>

interface RepositoryApi {
    fun commit(commit: String): CommitApi
    fun pipelines(): PipelinesApi
}

interface PipelinesApi {
    fun page(pageNumber: Int = 1): CompletableFuture<CloudPage<Pipeline>>
}


class BitbucketCloudApiConfig {
    var instance: BitbucketCloud = Production
    var authentication: Authentication = Guest
}

sealed class BitbucketCloud(
    val hostname: String,
    val apiHostname: String
) {
    val name: String = javaClass.simpleName
    val baseUrl: URI = URI.create("https://$hostname/")
    val apiBaseUrl: URI = URI.create("https://$apiHostname/")

    object Production : BitbucketCloud("bitbucket.org", "api.bitbucket.org")

    object Staging : BitbucketCloud("staging.bb-inf.net", "api-staging.bb-inf.net")
}

sealed class RepositoryId {
    abstract val urlPresentation: String
    abstract val displayPresentation: String

    data class WorkspaceAndSlug(val workspace: String, val slug: String) : RepositoryId() {
        override val urlPresentation = "$workspace/$slug"
        override val displayPresentation = urlPresentation
    }

    data class Uuid(val uuid: UUID) : RepositoryId() {
        override val urlPresentation = "{}/{$uuid}"
        override val displayPresentation = "{$uuid}"
    }
}

inline class Link(val href: URI)

data class Pipeline(val uuid: UUID)
