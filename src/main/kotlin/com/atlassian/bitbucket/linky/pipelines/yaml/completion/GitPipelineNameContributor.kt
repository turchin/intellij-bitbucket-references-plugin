package com.atlassian.bitbucket.linky.pipelines.yaml.completion

import com.atlassian.bitbucket.linky.LinkyBundle.message
import com.atlassian.bitbucket.linky.pipelines.yaml.pattern.inPipelinesFilePattern
import com.atlassian.bitbucket.linky.pipelines.yaml.pattern.withinYamlObject
import com.atlassian.bitbucket.linky.pipelines.yaml.vcs.repositoryForFile
import com.intellij.codeInsight.completion.*
import com.intellij.codeInsight.lookup.LookupElementBuilder
import com.intellij.icons.AllIcons
import com.intellij.openapi.application.ApplicationManager
import com.intellij.openapi.application.ex.ApplicationUtil
import com.intellij.openapi.progress.ProgressIndicatorProvider
import com.intellij.patterns.PlatformPatterns.psiElement
import com.intellij.util.ProcessingContext
import git4idea.branch.GitBranchUtil
import git4idea.repo.GitRepository

class GitPipelineNameContributor : CompletionContributor() {
    init {
        extend(CompletionType.BASIC, branchNameCompletionPattern, GitBranchNameCompletionProvider)
        extend(CompletionType.BASIC, tagNameCompletionPattern, GitTagNameCompletionProvider)
    }
}

private val branchNameCompletionPattern =
    psiElement()
        .and(inPipelinesFilePattern)
        .andOr(
            withinYamlObject("branches"),
            withinYamlObject("pull-requests")
        )

private val tagNameCompletionPattern =
    psiElement()
        .and(inPipelinesFilePattern)
        .and(withinYamlObject("tags"))

abstract class GitCompletionProvider : CompletionProvider<CompletionParameters>() {
    override fun addCompletions(
        parameters: CompletionParameters,
        context: ProcessingContext,
        result: CompletionResultSet
    ) {
        val repository = repositoryForFile(parameters.originalFile) as? GitRepository ?: return
        addCompletions(repository, result)
    }

    abstract fun addCompletions(repository: GitRepository, result: CompletionResultSet)
}

object GitBranchNameCompletionProvider : GitCompletionProvider() {
    override fun addCompletions(
        repository: GitRepository,
        result: CompletionResultSet
    ) {
        val branches = repository.branches
        result.addAllElements(
            branches.localBranches.map {
                LookupElementBuilder.create(it.name)
                    .withIcon(AllIcons.Vcs.Branch)
            }
        )
        result.addAllElements(
            branches.remoteBranches.map {
                LookupElementBuilder.create(it.nameForRemoteOperations)
                    .withIcon(AllIcons.Vcs.Branch)
                    .withTypeText(message("completion.remote.branch.hint"))
            }
        )
    }
}

object GitTagNameCompletionProvider : GitCompletionProvider() {
    override fun addCompletions(
        repository: GitRepository,
        result: CompletionResultSet
    ) {
        val future = ApplicationManager.getApplication()
            .executeOnPooledThread<List<String>> {
                GitBranchUtil.getAllTags(repository.project, repository.root)
            }

        val tags = ApplicationUtil.runWithCheckCanceled(
            future,
            ProgressIndicatorProvider.getInstance().progressIndicator
        )

        result.addAllElements(
            tags.map {
                LookupElementBuilder.create(it)
                    .withIcon(AllIcons.General.Balloon)
            }
        )
    }
}
