package com.atlassian.bitbucket.linky.actions.prlinks

import com.atlassian.bitbucket.linky.BitbucketRepository
import com.atlassian.bitbucket.linky.Revision
import com.atlassian.bitbucket.linky.actions.LinkyCommitActionContext
import com.atlassian.bitbucket.linky.actions.LinkyFileActionContext
import com.atlassian.bitbucket.linky.actions.linkyActionContext
import com.atlassian.bitbucket.linky.actions.logActionUsage
import com.atlassian.bitbucket.linky.gutter.DefaultGutterCommitDataProvider
import com.atlassian.bitbucket.linky.gutter.GutterCommitDataProvider
import com.atlassian.bitbucket.linky.icons.BitbucketLinkyIcons.Actions.PullRequestFind
import com.atlassian.bitbucket.linky.logger
import com.atlassian.bitbucket.linky.repository.hasRevisionBeenPushed
import com.atlassian.bitbucket.linky.rest.BitbucketRestException
import com.atlassian.bitbucket.linky.rest.cloud.PullRequestLinksIndexingException
import com.atlassian.bitbucket.linky.rest.cloud.PullRequestLinksNotEnabledException
import com.atlassian.bitbucket.linky.rest.cloud.oauth.OAuthException
import com.atlassian.bitbucket.linky.rest.onBitbucketError
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.application.ApplicationManager
import com.intellij.openapi.progress.ProgressIndicator
import com.intellij.openapi.progress.Task
import com.intellij.openapi.project.DumbAwareAction
import com.intellij.openapi.project.Project
import com.intellij.openapi.vcs.annotate.AnnotationGutterActionProvider
import com.intellij.openapi.vcs.annotate.FileAnnotation
import com.intellij.openapi.wm.ToolWindow
import com.intellij.openapi.wm.ToolWindowId
import com.intellij.openapi.wm.ToolWindowManager
import com.intellij.ui.content.Content
import com.intellij.ui.content.ContentFactory
import java.io.IOException
import java.util.concurrent.CompletableFuture

private val log = logger()

open class FindCommitRelatedPullRequestsAction : DumbAwareAction(
    "Find related Pull Requests", "Find Pull Requests that reviewed the selected commit", PullRequestFind
) {

    override fun update(event: AnActionEvent) {
        val repositoryRevision = getBitbucketRepositoryRevision(event)
        if (repositoryRevision == null) {
            event.presentation.isEnabledAndVisible = false
        } else {
            val (bbRepo, revision) = repositoryRevision
            event.presentation.isEnabledAndVisible = bbRepo.repository.hasRevisionBeenPushed(revision)
        }
    }

    override fun actionPerformed(event: AnActionEvent) {
        val (bitbucketRepository, revision) = getBitbucketRepositoryRevision(event) ?: return
        val project = bitbucketRepository.repository.project

        object : Task.Backgroundable(project, "Looking for related pull requests...") {
            override fun run(indicator: ProgressIndicator) {
                getRelatedPullRequests(bitbucketRepository, revision)
                    .thenApply {
                        it?.let { list ->
                            handleRelatedPullRequests(list, bitbucketRepository, revision)
                            logActionUsage("FindRelatedPullRequests", event.place, bitbucketRepository)
                        }
                    }
                    .onBitbucketError { error -> handleError(error, bitbucketRepository, revision) }
                    .join()
            }
        }.queue()
    }

    internal open fun getBitbucketRepositoryRevision(event: AnActionEvent): Pair<BitbucketRepository, Revision>? {
        val ctx = event.linkyActionContext()
        val bitbucketRepository = ctx.getBitbucketRepository() ?: return null
        return when (ctx) {
            is LinkyCommitActionContext -> Pair(bitbucketRepository, ctx.revision)
            is LinkyFileActionContext -> Pair(bitbucketRepository, ctx.linkyFile.revision)
            else -> null
        }
    }

    private fun getRelatedPullRequests(
        repository: BitbucketRepository,
        revision: Revision
    ): CompletableFuture<List<RelatedPullRequest>?> =
        try {
            when (repository) {
                is BitbucketRepository.Cloud -> fetchRelatedPullRequests(repository, revision)
                is BitbucketRepository.Server -> fetchRelatedPullRequests(repository, revision)
            }
        } catch (e: Exception) {
            throw FetchRelatedPullRequestsException(e)
        }

    private fun handleRelatedPullRequests(
        pullRequests: List<RelatedPullRequest>,
        repository: BitbucketRepository,
        revision: Revision
    ) {
        ApplicationManager.getApplication().invokeLater {
            val prPanel = showRelatedPullRequestsToolWindow(repository.repository.project, revision)
            prPanel.showPullRequests(pullRequests.sortedBy { it.id })
        }
    }

    private fun handleError(error: BitbucketRestException, repository: BitbucketRepository, revision: Revision) {
        val project = repository.repository.project
        ApplicationManager.getApplication().invokeLater {
            val prPanel = showRelatedPullRequestsToolWindow(project, revision)
            when (error) {
                is PullRequestLinksIndexingException -> prPanel.showPullRequestsAreBeingIndexed()
                is PullRequestLinksNotEnabledException -> prPanel.showPullRequestsNeedToBeIndexed(
                    repository as BitbucketRepository.Cloud,
                    revision
                )
                else -> {
                    when (val cause = error.cause) {
                        is OAuthException -> {
                            val oAuthCause = cause.cause ?: cause
                            if (oAuthCause is IOException) {
                                prPanel.showNetworkError()
                            } else {
                                prPanel.showAuthenticationRequiredError()
                            }
                        }
                        is IOException -> prPanel.showNetworkError().also {
                            log.debug("Failed to fetch related Pull Requests for $repository", error)
                        }
                        else -> prPanel.showGenericError().also {
                            log.error("Failed to fetch related Pull Requests for $repository", error)
                        }
                    }
                }
            }
        }
    }

    private fun showRelatedPullRequestsToolWindow(project: Project, revision: Revision): CommitPullRequestsPanel {
        val vcsToolWindow = ToolWindowManager.getInstance(project).getToolWindow(ToolWindowId.VCS)
        val (commitPullRequestsPanel, content) = vcsToolWindow.getOrCreatePullRequestsPanel(revision)

        commitPullRequestsPanel.showLoading()
        vcsToolWindow.contentManager.setSelectedContent(content, true)
        if (vcsToolWindow.isActive.not()) {
            vcsToolWindow.activate(null)
        }

        return commitPullRequestsPanel
    }

    private fun ToolWindow.getOrCreatePullRequestsPanel(revision: Revision): Pair<CommitPullRequestsPanel, Content> {
        val revisionDisplayName = revision.substring(0, minOf(revision.length, 7))
        val tabTitle = "Pull Requests: $revisionDisplayName"

        val existingContent = contentManager.contents.firstOrNull { it.displayName == tabTitle }
        return if (existingContent != null) {
            existingContent.component as CommitPullRequestsPanel to existingContent
        } else {
            val commitPullRequestsPanel = CommitPullRequestsPanel(contentManager)
            val content = ContentFactory.SERVICE.getInstance()
                .createContent(commitPullRequestsPanel, tabTitle, true)
            contentManager.addContent(content)
            commitPullRequestsPanel to content
        }
    }
}

class FindCommitRelatedPullRequestsGutterActionProvider : AnnotationGutterActionProvider {
    override fun createAction(annotation: FileAnnotation): AnAction =
        object : FindCommitRelatedPullRequestsAction(),
            GutterCommitDataProvider by DefaultGutterCommitDataProvider(annotation) {

            override fun getBitbucketRepositoryRevision(event: AnActionEvent): Pair<BitbucketRepository, Revision>? {
                val (bitbucketRepository, _) = super.getBitbucketRepositoryRevision(event) ?: return null
                val (_, _, lineRevision) = getGutterCommitData(event) ?: return null
                return lineRevision?.let { Pair(bitbucketRepository, lineRevision) }
            }
        }
}

private class FetchRelatedPullRequestsException(override val cause: Throwable) : RuntimeException(cause)
