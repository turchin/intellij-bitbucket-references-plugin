package com.atlassian.bitbucket.linky.stats

import com.atlassian.bitbucket.linky.SETTINGS_STORAGE_FILE
import com.atlassian.bitbucket.linky.logger
import com.atlassian.bitbucket.linky.utils.getString
import com.intellij.openapi.application.ApplicationInfo
import com.intellij.openapi.application.ApplicationNamesInfo
import com.intellij.openapi.components.*
import com.intellij.openapi.util.SystemInfo
import org.jdom.Element
import java.time.Duration
import java.time.Instant
import java.util.*
import kotlin.concurrent.fixedRateTimer

private val log = logger()

// Slightly delay analytics after busy IDE startup time
private val REPORT_ANALYTICS_DELAY = Duration.ofMinutes(1)
// Stats are saved locally together with all configuration options,
// so there's no need to report them frequently
private val REPORT_ANALYTICS_PERIOD = Duration.ofHours(1)

class LinkyAnalyticsService(private val amplitudeClient: AmplitudeClient) : BaseComponent {

    override fun getComponentName(): String = "BitbucketLinkyAnalytics"

    override fun initComponent() {
        fixedRateTimer(
            "analyticsSender",
            true,
            REPORT_ANALYTICS_DELAY.toMillis(),
            REPORT_ANALYTICS_PERIOD.toMillis()
        ) {
            reportInstalled()
            reportEvents()
        }
    }

    private fun reportInstalled() {
        val instance = service<LinkyInstanceState>()
        if (linkyVersion != instance.versionReported) {
            val installedEvent = createEvent("InstalledVersion")
            amplitudeClient.sendEvents(listOf(installedEvent))
                .thenAccept { instance.versionReported = linkyVersion }
                .whenComplete { _, error ->
                    error?.let { log.debug("Failed to report installed version", it) }
                }
        }
    }

    private fun reportEvents() {
        val statsService = service<UsageLog>() as MutableUsageLog
        val events = statsService.getEvents()
        // Amplitude allows no more than 10 events per second for a given user
        events.chunked(10)
            .map { eventsChunk ->
                val amplitudeEvents = eventsChunk.map {
                    createEvent(it.event, it.timestamp, eventProperties = it.attributes + mapOf("place" to it.place))
                }
                amplitudeClient.sendEvents(amplitudeEvents)
                    .thenAccept { statsService.removeEvents(eventsChunk) }
                    .whenComplete { _, error ->
                        error?.let { log.debug("Failed to report analytic events", it) }
                    }
            }
    }

    private fun createEvent(
        eventType: String,
        timestamp: Instant = Instant.now(),
        eventProperties: Map<String, String> = mapOf(),
        userProperties: Map<String, String> = mapOf()
    ): AmplitudeEvent {
        return with(service<ApplicationInfo>()) {
            AmplitudeEvent(
                deviceId = service<LinkyInstanceState>().instanceIdentifier,
                eventType = eventType,
                time = timestamp,
                platform = ApplicationNamesInfo.getInstance().fullProductName,
                appVersion = linkyVersion,
                osName = SystemInfo.OS_NAME,
                osVersion = SystemInfo.OS_VERSION,
                eventProperties = eventProperties + Pair("platform_version", "$fullVersion ${build.asString()}"),
                userProperties = userProperties
            )
        }
    }
}

@State(name = "Instance", storages = [Storage(SETTINGS_STORAGE_FILE)])
class LinkyInstanceState : PersistentStateComponent<Element> {
    internal var instanceIdentifier: String = UUID.randomUUID().toString()
        private set
    internal var versionReported: String? = null

    override fun getState() =
        Element("state").apply {
            setAttribute("id", instanceIdentifier)
            if (versionReported != null) {
                setAttribute("version", versionReported)
            }
        }

    override fun loadState(element: Element) {
        instanceIdentifier = element.getString("id") ?: instanceIdentifier
        versionReported = element.getString("version")
    }
}
