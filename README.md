# Bitbucket Linky plugin for IntelliJ Platform #

**Bitbucket/Stash References** plugin is now called **Bitbucket Linky**

This is a plugin for [IntelliJ IDEA Platform](https://www.jetbrains.com/idea/plugins/) that provides a handy way to navigate to
[Atlassian Bitbucket](https://www.atlassian.com/software/bitbucket).

It adds following IDE actions for projects hosted on Bitbucket:

* copy Bitbucket link to the selected file or selected lines in the file (`Ctrl+Shift+X,C` on Windows / `Cmd+Shift+X,C` on Mac)
* open selected file or selected lines in the file in Bitbucket in your Browser (`Ctrl+Shift+X,B` on Windows / `Cmd+Shift+X,B` on Mac)
* copy Bitbucket link to the selected commit
* open selected commit in Bitbucket in your Browser
* open _Create pull request_ form in Bitbucket for current branch (`Ctrl+Shift+X,P` on Windows / `Cmd+Shift+X,P` on Mac)
* create Snippet in Bitbucket Cloud (`Ctrl+Shift+X,S` on Windows / `Cmd+Shift+X,S` on Mac)
* look up Pull Requests which included the given commit

Bitbucket Linky supports both **Bitbucket Cloud** and **Bitbucket Server** (formerly known as **Stash**).
                
The plugin uses your project's VCS settings (supports both Git and Mercurial) to build links 
to the repository in Bitbucket, and in most cases doesn't require configuration. However, if
Bitbucket hosting is not automatically recognized, it can be configured via IDE preferences.

#### Please note that this plugin is not officially supported by Atlassian.

## Development

To build the plugin:

* install `gradle`
* run `gradle clean build`

To run IDEA with the plugin:

* run `gradle runIde` or use that gradle task to create a run/debug configuration in IDEA

## Publishing the plugin

In order to publish Linky you need to configure your credentials for [https://hub.jetbrains.com/](https://hub.jetbrains.com/):

```bash
gradle addCredentials --key linkyPublishUsername --value {username} -PcredentialsPassphrase={passphrase}
gradle addCredentials --key linkyPublishPassword --value {password} -PcredentialsPassphrase={passphrase}
```

This will encrypt and save provided username and password in `$HOME/.gralde/gradle.{md5OfYourPassphrase}.encrypted.properties`.

To publish the plugin:

```bash
gradle -PcredentialsPassphrase={passphrase} clean publishPlugin
```

## License and Copyright

Please see the included [license file](META-INF/license.txt) for details.

Portions of this code are derived from [Crucible4IDEA](https://github.com/ktisha/Crucible4IDEA/)
and are copyright (c) 2013-2015 Ekaterina Tuzova (ktisha)

Huge thanks to **Brent Plump**, **Zaki Salleh**, **Piotr Wilczyński** and **Gerry Tan**!
