package com.atlassian.bitbucket.linky.preferences

import com.atlassian.bitbucket.linky.logger
import com.intellij.dvcs.repo.Repository
import com.intellij.openapi.application.ApplicationManager
import com.intellij.openapi.progress.ProgressIndicator
import com.intellij.openapi.progress.ProgressManager
import com.intellij.openapi.progress.Task
import git4idea.commands.Git
import git4idea.repo.GitRepository

class GitRepositoryPreferences : RepositoryPreferences {
    override fun preferences(repository: Repository): Preferences? =
        when (repository) {
            is GitRepository -> GitConfigPreferences(repository)
            else -> null
        }
}

private val log = logger()

private class GitConfigPreferences(private val repository: GitRepository) : Preferences {

    private val prefix = "bitbucket-linky."
    private lateinit var properties: MutableMap<String, String>

    init {
        val doLoadProperties = { properties = loadProperties() }
        if (ApplicationManager.getApplication().isDispatchThread) {
            ProgressManager.getInstance().runProcessWithProgressSynchronously(
                doLoadProperties,
                "Loading repository properties...",
                false,
                repository.project
            )
        } else {
            doLoadProperties()
        }
    }

    override fun getProperty(key: String, defaultValue: String?): String? = properties.getOrDefault(key, defaultValue)

    override fun setProperty(key: String, value: String) {
        properties[key] = value
        updateProperties("$prefix$key", value)
    }

    override fun removeProperty(key: String) {
        properties.remove(key)
        updateProperties("--unset", "$prefix$key")
    }

    private fun loadProperties(): MutableMap<String, String> {
        val result = Git.getInstance().config(repository, "--local", "--list")
        if (result.success()) {
            return result.output
                .filter { it.startsWith(prefix) }
                .map { it.removePrefix(prefix) }
                .mapNotNull { it.parseKeyValue() }
                .toMap(mutableMapOf())
        } else {
            throw IllegalStateException("Failed to load Git repository properties: ${result.errorOutputAsJoinedString}")
        }
    }

    private fun updateProperties(vararg params: String) {
        object : Task.Backgroundable(repository.project, "Updating repository properties...", false) {
            override fun run(indicator: ProgressIndicator) {
                val result = Git.getInstance().config(repository, "--local", *params)
                if (!result.success()) {
                    log.error("Failed to update Git repository properties with parameters '${params.contentToString()}': ${result.errorOutputAsJoinedString}")
                }
            }
        }.queue()
    }
}
