package com.atlassian.bitbucket.linky.rest.server

import com.atlassian.bitbucket.linky.rest.auth.Authentication.*
import com.atlassian.bitbucket.linky.rest.auth.RequestAuthenticationInterceptorBase
import com.atlassian.bitbucket.linky.rest.auth.basicAuthInterceptor
import com.atlassian.bitbucket.linky.rest.server.config.defaultGson
import com.github.kittinunf.fuel.core.FoldableRequestInterceptor
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.core.Request
import com.github.kittinunf.fuel.core.RequestTransformer
import com.github.kittinunf.fuel.core.extensions.authentication
import com.google.gson.Gson

class BitbucketServerApiImpl(
    createFuel: () -> FuelManager = { FuelManager() },
    private val gson: Gson = defaultGson(),
    customConfig: BitbucketServerApiConfig.() -> Unit = { }
) : BitbucketServerApi {

    private val config = BitbucketServerApiConfig().apply(customConfig)
    private val fuel = createFuel()

    init {
        with(config) {
            when (val auth = authentication) {
                is Basic -> {
                    fuel.addRequestInterceptor(basicAuthInterceptor(instance, auth))
                }
                is AccessToken -> {
                    fuel.addRequestInterceptor(bearerAuthInterceptor(instance, auth))
                }
                is OAuth -> throw IllegalArgumentException("Bitbucket Server doesn't OAuth authentication")
                else -> fuel
            }
        }
    }

    override fun personalAccessTokens(username: String): PersonalAccessTokensApi =
        PersonalAccessTokensApiImpl(fuel, gson, config.instance, username)

    override fun repository(repositoryId: RepositoryId): RepositoryApi =
        RepositoryApiImpl(fuel, gson, config.instance, repositoryId)

    override fun testConnectivity(): ConnectivityTestApi =
        ConnectivityTestApiImpl(fuel, gson, config.instance)
}

private fun bearerAuthInterceptor(server: BitbucketServer, auth: AccessToken): FoldableRequestInterceptor =
    object : RequestAuthenticationInterceptorBase(server) {
        override fun intercept(next: RequestTransformer, request: Request): Request =
            next(request.authentication().bearer(auth.token.value))
    }
