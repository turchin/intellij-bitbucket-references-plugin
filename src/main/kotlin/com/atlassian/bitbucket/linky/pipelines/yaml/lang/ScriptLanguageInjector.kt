package com.atlassian.bitbucket.linky.pipelines.yaml.lang

import com.atlassian.bitbucket.linky.pipelines.yaml.pattern.inPipelinesFilePattern
import com.atlassian.bitbucket.linky.pipelines.yaml.pattern.yamlKeyValuePattern
import com.intellij.patterns.ElementPattern
import com.intellij.patterns.PlatformPatterns.psiElement
import com.intellij.psi.ElementManipulators
import com.intellij.psi.InjectedLanguagePlaces
import com.intellij.psi.LanguageInjector
import com.intellij.psi.PsiLanguageInjectionHost
import com.intellij.sh.ShLanguage
import org.jetbrains.yaml.psi.YAMLScalar
import org.jetbrains.yaml.psi.YAMLSequenceItem

class ScriptLanguageInjector : LanguageInjector {
    override fun getLanguagesToInject(
        host: PsiLanguageInjectionHost,
        injectionPlacesRegistrar: InjectedLanguagePlaces
    ) {
        if (taskOfScriptPattern.accepts(host)) {
            val range = ElementManipulators.getValueTextRange(host)
            if (!range.isEmpty) {
                injectionPlacesRegistrar.addPlace(ShLanguage.INSTANCE, range, null, null)
            }
        }
    }
}

private val taskOfScriptPattern: ElementPattern<YAMLScalar> =
    psiElement(YAMLScalar::class.java)
        .and(inPipelinesFilePattern)
        .withParent(YAMLSequenceItem::class.java)
        .withSuperParent(
            3, psiElement().andOr(
                yamlKeyValuePattern("script"),
                yamlKeyValuePattern("after-script")
            )
        )
