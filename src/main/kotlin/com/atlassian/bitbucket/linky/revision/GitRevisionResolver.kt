package com.atlassian.bitbucket.linky.revision

import com.atlassian.bitbucket.linky.Revision
import com.intellij.openapi.vcs.history.VcsRevisionNumber
import git4idea.GitRevisionNumber

class GitRevisionResolver : VcsRevisionResolver {
    override fun resolveVcsRevision(vcsRevisionNumber: VcsRevisionNumber): Revision? =
        when (vcsRevisionNumber) {
            is GitRevisionNumber -> vcsRevisionNumber.rev
            else -> null
        }
}
