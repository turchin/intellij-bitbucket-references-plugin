package com.atlassian.bitbucket.linky.rest.cloud.config

import com.atlassian.bitbucket.linky.generateServiceName
import com.atlassian.bitbucket.linky.rest.auth.OAuthTokens
import com.atlassian.bitbucket.linky.rest.auth.OAuthTokens.RefreshAndAccessTokens
import com.atlassian.bitbucket.linky.rest.auth.OAuthTokens.RefreshTokenOnly
import com.atlassian.bitbucket.linky.rest.auth.RefreshToken
import com.atlassian.bitbucket.linky.rest.cloud.BitbucketCloud
import com.intellij.credentialStore.CredentialAttributes
import com.intellij.credentialStore.Credentials
import com.intellij.ide.passwordSafe.PasswordSafe
import com.intellij.openapi.components.service
import java.util.concurrent.atomic.AtomicReference

interface OAuthTokensHolder {
    fun getOAuthTokens(instance: BitbucketCloud): OAuthTokens?

    fun saveOAuthTokens(instance: BitbucketCloud, oAuthTokens: RefreshAndAccessTokens)
}

private const val TOKENS_DELIMITER = " : "

class DefaultOAuthTokensHolder : OAuthTokensHolder {

    private val tokens = AtomicReference<OAuthTokens?>()

    override fun getOAuthTokens(instance: BitbucketCloud): OAuthTokens? {
        val maybeTokens = tokens.get()
        return maybeTokens ?: loadRefreshToken(instance).also {
            tokens.compareAndSet(maybeTokens, it)
        }
    }

    override fun saveOAuthTokens(instance: BitbucketCloud, oAuthTokens: RefreshAndAccessTokens) {
        tokens.set(oAuthTokens)
        saveRefreshToken(instance, oAuthTokens.refreshToken)
    }

    private fun loadRefreshToken(instance: BitbucketCloud): RefreshTokenOnly? {
        val passwordSafe = service<PasswordSafe>()
        val tokenString = passwordSafe.getPassword(credentialsAttributes(instance)) ?: return null
        val refreshToken = tokenString.substringBefore(TOKENS_DELIMITER)
        return RefreshTokenOnly(RefreshToken(refreshToken))
    }

    private fun saveRefreshToken(instance: BitbucketCloud, refreshToken: RefreshToken) {
        val passwordSafe = service<PasswordSafe>()
        passwordSafe.set(credentialsAttributes(instance), Credentials(instance.name, refreshToken.value))
    }

    private fun credentialsAttributes(cloud: BitbucketCloud): CredentialAttributes {
        val name = cloud.name
        return CredentialAttributes(generateServiceName("Bitbucket Cloud $name"), name, null, false)
    }
}
