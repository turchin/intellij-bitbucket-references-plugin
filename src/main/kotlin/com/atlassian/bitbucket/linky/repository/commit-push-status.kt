package com.atlassian.bitbucket.linky.repository

import com.atlassian.bitbucket.linky.Revision
import com.atlassian.bitbucket.linky.logger
import com.intellij.dvcs.repo.Repository
import com.intellij.openapi.components.ProjectComponent
import com.intellij.openapi.components.service
import com.intellij.openapi.progress.util.BackgroundTaskUtil
import com.intellij.openapi.project.Project
import com.intellij.openapi.vfs.VirtualFile
import git4idea.commands.Git
import git4idea.commands.GitCommand
import git4idea.commands.GitLineHandler
import git4idea.repo.GitRepository
import git4idea.repo.GitRepositoryChangeListener
import org.zmlx.hg4idea.HgUpdater
import org.zmlx.hg4idea.HgVcs
import org.zmlx.hg4idea.execution.HgRemoteCommandExecutor
import java.util.concurrent.ConcurrentHashMap

private val log = logger()

class GitRevisionPushStatusReporter(private val project: Project) : ProjectComponent {

    private val outgoingRevisionsMap = ConcurrentHashMap<VirtualFile, Set<Revision>>()
    private val git by lazy { service<Git>() }

    override fun initComponent() {
        project.messageBus.connect()
            .subscribe(GitRepository.GIT_REPO_CHANGE, GitRepositoryChangeListener { gitRepository ->
                updateOutgoingRevisions(project, gitRepository.root)
            })
    }

    fun hasRevisionBeenPushed(repository: Repository, revision: Revision) =
        outgoingRevisionsMap.getOrDefault(repository.root, setOf()).contains(revision).not()

    private fun updateOutgoingRevisions(project: Project, root: VirtualFile) {
        val lineHandler = GitLineHandler(project, root, GitCommand.LOG).apply {
            addParameters("--branches", "--not", "--remotes", "--pretty=format:%H")
        }

        val result = git.runCommand(lineHandler)

        if (result.success()) {
            outgoingRevisionsMap[root] = result.output.toSet()
        }
    }
}

class HgRevisionPushStatusReporter(private val project: Project) : ProjectComponent {
    private val outgoingRevisionsMap = ConcurrentHashMap<VirtualFile, Set<Revision>>()
    private val revisionPrefix = "#-"

    override fun initComponent() {
        project.messageBus.connect()
            .subscribe(HgVcs.STATUS_TOPIC, HgUpdater { project, root ->
                root?.let { updateOutgoingRevisions(project, it) }
            })
    }

    fun hasRevisionBeenPushed(repository: Repository, revision: Revision) =
        outgoingRevisionsMap.getOrDefault(repository.root, setOf()).contains(revision).not()

    private fun updateOutgoingRevisions(project: Project, root: VirtualFile) {
        log.debug("Updating outgoing revisions for Mercurial repository at ${root.path}")
        BackgroundTaskUtil.executeOnPooledThread(project, Runnable {
            val executor = HgRemoteCommandExecutor(project, null, null, true)
            val result = executor.executeInCurrentThread(
                root,
                "outgoing",
                listOf("--template", "$revisionPrefix{node}\n")
            )
            val outgoingRevisions = result
                ?.outputLines
                ?.filter { it.startsWith(revisionPrefix) }
                ?.map { it.removePrefix(revisionPrefix) }
                ?.toSet()
                ?: emptySet()
            outgoingRevisionsMap[root] = outgoingRevisions
        })
    }
}
