package com.atlassian.bitbucket.linky.pipelines.yaml.reference

import com.atlassian.bitbucket.linky.pipelines.yaml.resolve.resolveCacheDefinitions
import com.atlassian.bitbucket.linky.pipelines.yaml.resolve.resolveServiceDefinitions
import com.intellij.codeInsight.lookup.LookupElementBuilder
import com.intellij.icons.AllIcons
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiReferenceBase
import com.intellij.util.containers.MultiMap
import org.jetbrains.yaml.psi.YAMLKeyValue
import org.jetbrains.yaml.psi.YAMLScalar


sealed class DefinitionReference(scalar: YAMLScalar) : PsiReferenceBase<YAMLScalar>(scalar) {
    abstract val predefinedNames: List<String>

    protected val name = scalar.textValue

    override fun getVariants(): Array<Any> {
        val userDefinedDefinitions = definitionsMap().keySet().toList()
        val missingPredefinedDefinitions = predefinedNames.filter { it !in userDefinedDefinitions }

        val options =
            userDefinedDefinitions.map { lookupElementBuilder(it, true) } +
                    missingPredefinedDefinitions.map { lookupElementBuilder(it, false) }

        return options.toTypedArray()
    }

    override fun resolve(): PsiElement? = definitionsMap()[name].firstOrNull()

    protected abstract fun definitionsMap(): MultiMap<String, YAMLKeyValue>

    protected abstract fun lookupElementBuilder(name: String, userDefined: Boolean): LookupElementBuilder
}

class CacheDefinitionReference(scalar: YAMLScalar) : DefinitionReference(scalar) {
    override val predefinedNames = listOf(
        "docker", "composer", "dotnetcore", "gradle", "ivy2", "maven", "node", "pip", "sbt"
    )

    override fun definitionsMap() = resolveCacheDefinitions(element.containingFile)

    override fun lookupElementBuilder(name: String, userDefined: Boolean): LookupElementBuilder =
        LookupElementBuilder.create(name)
            .withIcon(AllIcons.Nodes.Class)
            .withBoldness(userDefined)
            .withItemTextItalic(!userDefined)

    override fun isSoft(): Boolean = predefinedNames.contains(name)
}

class ServiceDefinitionReference(scalar: YAMLScalar) : DefinitionReference(scalar) {
    override val predefinedNames = predefinedServices

    override fun definitionsMap() = resolveServiceDefinitions(element.containingFile)

    override fun lookupElementBuilder(name: String, userDefined: Boolean): LookupElementBuilder =
        LookupElementBuilder.create(name)
            .withIcon(AllIcons.Nodes.Static)
            .withBoldness(userDefined)
            .withItemTextItalic(!userDefined)

    override fun isSoft(): Boolean = predefinedServices.contains(name)

    companion object {
        val predefinedServices = listOf("docker")
    }
}
