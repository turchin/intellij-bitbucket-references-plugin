package com.atlassian.bitbucket.linky.util

import assertk.assertThat
import assertk.assertions.isEqualTo
import assertk.assertions.isNull
import com.atlassian.bitbucket.linky.selection.LinesSelection
import com.atlassian.bitbucket.linky.selection.compileStringReference
import org.junit.jupiter.api.Test

internal class LineSelectionsTest {

    @Test
    fun `test empty selections list`() {
        assertThat(collectWithDefaultDelimiters(emptyList())).isNull()
    }

    @Test
    fun `test merge adjacent intervals`() {
        assertThat(
            collectWithDefaultDelimiters(
                listOf(
                    LinesSelection(1, 2),
                    LinesSelection(2, 3)
                )
            )
        ).isEqualTo("1:3")

        assertThat(
            collectWithDefaultDelimiters(
                listOf(
                    LinesSelection(1, 1),
                    LinesSelection(2, 2),
                    LinesSelection(4, 4)
                )
            )
        ).isEqualTo("1:2,4")
    }

    @Test
    fun `test merge overlapping intervals`() {
        assertThat(
            collectWithDefaultDelimiters(
                listOf(
                    LinesSelection(1, 3),
                    LinesSelection(2, 4)
                )
            )
        ).isEqualTo("1:4")

        assertThat(
            collectWithDefaultDelimiters(
                listOf(
                    LinesSelection(1, 6),
                    LinesSelection(3, 8),
                    LinesSelection(2, 4),
                    LinesSelection(4, 4)
                )
            )
        ).isEqualTo("1:8")
    }

    @Test
    fun `test no duplicate intervals in URL`() {
        assertThat(
            collectWithDefaultDelimiters(
                listOf(
                    LinesSelection(14, 16),
                    LinesSelection(19, 23),
                    LinesSelection(19, 20),
                    LinesSelection(21, 23),
                    LinesSelection(19, 23),
                    LinesSelection(25, 25),
                    LinesSelection(25, 25),
                    LinesSelection(27, 31)
                )
            )
        ).isEqualTo("14:16,19:23,25,27:31")
    }

    @Test
    fun `test order intervals`() {
        assertThat(
            collectWithDefaultDelimiters(
                listOf(
                    LinesSelection(12, 14),
                    LinesSelection(1, 2),
                    LinesSelection(22, 34),
                    LinesSelection(10, 10)
                )
            )
        ).isEqualTo("1:2,10,12:14,22:34")
    }

    @Test
    fun `test prefix, delimiter and suffix`() {
        val prefix = "PREFIX = ["
        val selectionsDelimiter = "];["
        val intervalSign = "/"
        val suffix = "]"
        assertThat(
            listOf(
                LinesSelection(1, 2),
                LinesSelection(5, 5),
                LinesSelection(8, 9)
            ).compileStringReference(
                prefix,
                selectionsDelimiter,
                intervalSign,
                suffix
            )
        ).isEqualTo("${prefix}1${intervalSign}2${selectionsDelimiter}5${selectionsDelimiter}8${intervalSign}9$suffix")
    }

    private fun collectWithDefaultDelimiters(linesSelections: List<LinesSelection>) =
        linesSelections.compileStringReference("", ",", ":", "")

}
